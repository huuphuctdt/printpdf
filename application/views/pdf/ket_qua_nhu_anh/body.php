<div class="pdf-body" style="font-size: 14px;">
    <div style="float: left; width: 60%; padding-top: 20px;">
        <div style="font-weight: bold; font-size: 20px; text-align: center;">KẾT QUẢ NHŨ ẢNH</div>
        <div style="font-weight: bold; font-size: 20px; text-align: center;">MAMMOGRAPHY RESULT</div>
    </div>
    <div style="float: left; text-align: center;">
        <div style="margin-top: 15px;">
            <div><barcode code="<?=$barcode;?>" type="EAN128C" /></div>
            <?php if(isset($barcode) && $barcode != ""){ ?>
                <div><span style="font-weight: bold;">Mã số (Code): </span> <?=$barcode;?></div>
            <?php } ?>
        </div>
    </div>
    <div style="clear: both;"></div>
    <hr/>
    <div style="float: left; width: 70%;">
        <span>Họ và tên(Full name) : </span>
        <span>Nguyễn Văn A</span>
    </div>
    <div style="float: left;">
        <span>Tuổi(Age) : 1988</span>
    </div>
    <div style="clear: both;"></div>
    <div><span>Phòng XQuang (XRay No) : </span><span>Phòng XQUANG SỐ 12</span></div>
    <div><span>Địa chỉ (Address) : </span><span>123 Trần Hưng Đạo, Quận 5, TPHCM</span></div>
    <div><span>Chẩn đoán lâm sàng(Clinical Diagnosis) : </span><span></span></div>
    <div><span>BS Chỉ Định (Treatment Doctor) : </span><span>NGUYỄN THỊ THANH TÂM</span></div>
    <hr/>

    <div style="float: left;width: 33%; text-align: center;">
        <div><span>VÚ PHẢI</span></div>
        <div><span>(Right)</span></div>
    </div>
    <div style="float: left;width: 33%; text-align: center;">
        <div>
            <span style="font-weight: bold;">
                PHÂN LOẠI THEO BI-RADS
            </span>
        </div>
        <div>
            <span style="font-weight: bold;">
                BI-RADS CLASSFICATION
            </span>
        </div>
    </div>
    <div style="float: left; text-align: center;">
        <div><span>VÚ TRÁI</span></div>
        <div><span>(left)</span></div>
    </div>
    <div style="clear: both"></div>

    <div style="float: left;width: 20%; text-align: center;">
        <div><img class="" alt="" src="<?php echo base_url('assets/images/black.jpg'); ?>"></div>
    </div>
    <div style="float: left;width: 60%; text-align: center;">
        <div style="float: left;width: 15%;">[]</div>
        <div style="float: left;width: 70%; text-align: left;">
            0. Cần khảo sát hình học bổ sung<br>
            (Further imaging procedures required)
        </div>
        <div style="float: left;">[]</div>
        <div style="clear: both"></div>
        <div style="float: left;width: 15%;">[]</div>
        <div style="float: left;width: 70%; text-align: left;">
            1. Không có bất thường <br>
            (No abnormality)
        </div>
        <div style="float: left;">[]</div>
        <div style="clear: both"></div>
        <div style="float: left;width: 15%;">[X]</div>
        <div style="float: left;width: 70%; text-align: left;">
            2. Sang thương lành tính <br>
            (Benign lesion)
        </div>
        <div style="float: left;">[X]</div>
        <div style="clear: both"></div>
        <div style="float: left;width: 15%;">[]</div>
        <div style="float: left;width: 70%; text-align: left;">
            3. Sang thương có thể lành tính <br>
            (Probably benign lesion)
        </div>
        <div style="float: left;">[]</div>
        <div style="clear: both"></div>
        <div style="float: left;width: 15%;">[]</div>
        <div style="float: left;width: 70%; text-align: left;">
            4. Sang thương có thể ác tính <br>
            (Probably malignant lesion)
        </div>
        <div style="float: left;">[]</div>
        <div style="clear: both"></div>
        <div style="float: left;width: 15%;">[]</div>
        <div style="float: left;width: 70%; text-align: left;">
            5. Sang thương ác tính <br>
            (Malignant lesion)
        </div>
        <div style="float: left;">[]</div>
        <div style="clear: both"></div>
        <div style="float: left;width: 15%;">[]</div>
        <div style="float: left;width: 70%; text-align: left;">
            6. Sang thương / kết quả sinh thiết ác tính <br>
            (Malignant lesion on biopsy)
        </div>
        <div style="float: left;">[]</div>
        <div style="clear: both"></div>
    </div>
    <div style="float: left; text-align: center;">
        <div><img class="" alt="" src="<?php echo base_url('assets/images/black.jpg'); ?>"></div>
    </div>
    <div style="clear: both"></div>

    <div style="float: left;width: 30%;">
        <div style="font-weight: bold;">Mô tả (Description): </div>
    </div>
    <div style="float: left;">
        <div>- Mô tuyến vú nhóm C</div>
        <div style="margin-left: 10px;">Breast glandular tissue: type C</div>
        <div>- Vú Phải: 1/4 trên ngoài có 1 nốt d # (0,6x0,5)cm, bờ rõ, cản quang trung bình, không kèm microcalci.
            Có vài microcalci rải rác, không kèm khối u.</div>
        <div style="margin-left: 10px;">Right breast: in the upper outer, a mas, size # (0,6x0,5)cm, sharpboundaries, medium density,
        no associated microcalci lession. Some microcalci, no associated mass.</div>
        <div>- Vú trái: 1/2 ngoài có 1 nốt d # (0,8x0,6)cmbờ rõ, cản quang trung bình, không kèm microcalci.
            Có vài microcalci rải rác, không kèm khối u.</div>
        <div style="margin-left: 10px;">Left breast: in the upper half, a mas, size # (0,8x0,6)cm, sharpboundaries, medium density,
            no associated microcalci lession. Some microcalci, no associated mass.</div>
        <div>- Hạch nách: phải (-), trái (+)</div>
        <div style="margin-left: 10px;">Axillary lympho nodes: right (-), left (+)</div>
    </div>
    <div style="clear: both"></div>

    <div style="float: left;width: 30%;">
        <span style="font-weight: bold;">Đề nghị (Suggestion): </span>
        <span>BREAST ULTRASOUND</span>
    </div>
    <div style="float: left;"></div>
    <div style="clear: both"></div>

    <div style="float: left;width: 60%;">&nbsp;</div>
    <div style="float: left">
        <div style="text-align: center; font-weight: bold;">Tp.HCM, Ngày 14 tháng 05 năm 2018</div>
        <div style="text-align: center; font-weight: bold;">Bác sĩ X-Quang/XRay Dr</div>
        <div style="text-align: center; font-weight: bold; margin-top: 80px;">VÕ THỊ THU HẰNG</div>
    </div>
    <div style="clear: both;"></div>
</div>