<div class="pdf-body">
    <div style="text-align: center; margin-top: -20px;">
        <h2>PHIẾU SOI CỔ TỬ CUNG</h2>
    </div>
    <div style="margin-top: -15px; text-align: center;">
        Ngày 26/05/2018
    </div>
    <div style="text-align: right;">
        <div><barcode code="<?=$barcode;?>" type="EAN128C" /></div>
        <?php if(isset($barcode) && $barcode != ""){ ?>
            <div><span style="font-weight: bold;">Mã số (Code): </span> <?=$barcode;?></div>
        <?php } ?>
    </div>
    <table>
        <tr>
            <td style="padding: 5px;"><span style="font-weight: bold;">Mã BN: </span> <span>18/014041</span></td>
            <td style="padding: 5px;"><span style="font-weight: bold;">Mã phụ: </span></td>
            <td style="padding: 5px;"></td>
        </tr>
        <tr>
            <td style="padding: 5px;"><span style="font-weight: bold;">Họ tên: </span> <span>CHU THỊ THỦY NGỌC</span></td>
            <td style="padding: 5px;"></td>
            <td style="padding: 5px;"><span style="font-weight: bold;">Phòng: </span> <span>Phòng soi lầu 5</span></td>
        </tr>
        <tr>
            <td style="padding: 5px;" colspan="3">
                <span style="font-weight: bold;">Địa chỉ: </span>
                <span>13 K1 KHU DÂN CƯ TÂN QUY ĐÔNG, PHƯỜNG TÂN PHONG, QUẬN 7, TPHCM</span>
            </td>
        </tr>
        <tr>
            <td style="padding: 5px;"><span style="font-weight: bold;">Năm sinh: </span> <span>1983</span></td>
            <td style="padding: 5px;"></td>
            <td style="padding: 5px;"><span style="font-weight: bold;">Điện thoại: </span> <span>0123456789</span></td>
        </tr>
        <tr>
            <td style="padding: 5px;"><span style="font-weight: bold;">Para: </span></td>
            <td style="padding: 5px;"></td>
            <td style="padding: 5px;"><span style="font-weight: bold;">Pap's mear: </span></td>
        </tr>
        <tr>
            <td style="padding: 5px;" colspan="3">
                <span style="font-weight: bold;">CĐ lâm sàng: </span>
                <span></span>
            </td>
        </tr>
        <tr>
            <td style="padding: 5px;" colspan="3">
                <span style="font-weight: bold;">BS Chỉ định: </span>
                <span style="font-weight: bold;">DƯƠNG THỊ BÍCH THẢO</span>
            </td>
        </tr>
    </table>
<!--    <table border="1" style="width: 100%;">-->
<!--        <tr>-->
<!--            <td style="text-align: center;">Mô tả</td>-->
<!--            <td style="text-align: center;">Hình ảnh</td>-->
<!--        </tr>-->
<!--        <tr>-->
<!--            <td>-->
<!--                <div>>ÂM HỘ: Bình thường.</div><br>-->
<!--                <div>>ÂM ĐẠO: Ít huyết trắng.</div><br>-->
<!--                <div>CỔ TỬ CUNG: 3 cm</div><br>-->
<!--                <div>-->
<!--                    <ol>-->
<!--                        <li>Biếu mô lát: Mặt ngoài láng.</li><br>-->
<!--                        <li>Biếu mô tuyến: lộ tuyến mép trên, dưới 0,5 cm.</li><br>-->
<!--                        <li>Vùng tiếp giáp lát - trụ: Lỗ ngòai.</li><br>-->
<!--                        <li>Mô đệm: sung huyết.</li><br>-->
<!--                    </ol>-->
<!--                </div><br>-->
<!--                <div>>SAU ACIDE: Không hinh ảnh bất thường.</div><br>-->
<!--                <div>>LUGOL: không bắt màu vùng lộ tuyến.</div><br>-->
<!--            </td>-->
<!--            <td>-->
<!--                <div><img class="" alt="" src="--><?php //echo base_url('assets/images/black.jpg'); ?><!--"></div>-->
<!--            </td>-->
<!--        </tr>-->
<!--    </table>-->
    <div style="width: 60%; float: left; text-align: center; font-weight: bold;">Mô tả</div>
    <div style="float: left; text-align: center; font-weight: bold;">Hình ảnh</div>
    <div style="clear: both;"></div>
    <div style="width: 60%; float: left; ">
        <div style="padding-left: 10px;">>ÂM HỘ: Bình thường.</div><br>
        <div style="padding-left: 10px;">>ÂM ĐẠO: Ít huyết trắng.</div><br>
        <div style="padding-left: 10px;">>CỔ TỬ CUNG: 3 cm</div><br>
        <div>
            <ol style="margin-top: -20px;">
                <li>Biếu mô lát: Mặt ngoài láng.</li>
                <li>Biếu mô tuyến: lộ tuyến mép trên, dưới 0,5 cm.</li>
                <li>Vùng tiếp giáp lát - trụ: Lỗ ngòai.</li>
                <li>Mô đệm: sung huyết.</li>
            </ol>
        </div>
        <div style="padding-left: 10px;">>SAU ACIDE: Không hinh ảnh bất thường.</div><br>
        <div style="padding-left: 10px;">>LUGOL: không bắt màu vùng lộ tuyến.</div><br>
    </div>
    <div style="float: left; ">
        <div style="margin-top: 50px;"><img class="" alt="" src="<?php echo base_url('assets/images/black.jpg'); ?>"></div>
    </div>
    <div style="clear: both;"></div>
    <div>
        <span style="font-weight: bold;">Kết luận: </span>
        <span> Lộ tuyến nhẹ CTC</span>
    </div>
    <div>
        <span style="font-weight: bold;">Xử trí: </span>
    </div>
    <div>
        <span style="font-weight: bold;">Đề nghị: </span>
    </div>

    <div style="float: left;width: 60%;">&nbsp;</div>
    <div style="float: left">
        <div style="text-align: center; font-weight: bold;">Ngày:26 tháng 05 năm 2018</div>
        <div style="text-align: center; font-weight: bold;">BÁC SĨ PHÒNG SOI</div>
        <div style="text-align: center; font-weight: bold; margin-top: 80px;">BS. TRẦN THỊ HAI</div>
    </div>
    <div style="clear: both;"></div>
</div>