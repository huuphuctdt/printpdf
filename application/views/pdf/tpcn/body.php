<div class="pdf-body">
    <div style="float: left;width: 50%; ">
        <div style="font-weight: bold; font-size: 30px;text-align: center;">TPCN - MP - VTYT</div>
        <div style="font-weight: bold; text-align: center; margin-top: -5px;">Ngày: 25/05/2018</div>
        <div style="margin-top: 15px;">
            <span style="font-weight: bold;">Mã BN: 14/048129</span>
        </div>
        <div style="font-weight: bold;margin-top: 10px;">Họ và tên: CHU THỊ THỦY NGỌC</div>
    </div>
    <div style="float: left;width: 50%; text-align: center;">
        <div><barcode code="<?=$barcode;?>" type="EAN128C" /></div>
        <div><span style="font-weight: bold;">Mã số (Code): </span> <?=$barcode;?></div>
        <div style="margin-top: 13px;"><span style="font-weight: bold;">Năm sinh:</span> 1989</div>
        <div style="margin-top: 12px;"><span style="font-weight: bold;">Giới tính:</span> Nữ</div>
    </div>
    <div style="clear: both;"></div>
    <div style="margin-top: 10px;"><span style="font-weight: bold;">Địa chỉ:</span> 13 LÔ C, ĐƯỜNG NGUYÊN VĂN PHÚ, PHƯỜNG 5, QUẬN 11, TPHCM</div>
    <div style="margin-top: 10px;"><span style="font-weight: bold;margin-top: 10px;">Chẩn đoán:</span></div>
    <table style="width: 100%; text-align: center;margin-top: 10px;">
        <tr>
            <td style="width: 10%;">1</td>
            <td style="width: 70%; text-align: left;">Peron 250ml (**)</td>
            <td style="width: 20%;">01 Chai</td>
        </tr>
    </table>
    <div style="text-align: center; margin-top: 10px;">Rửa âm hộ Sáng: 1 lần, Tối: 1 lần</div>
</div>