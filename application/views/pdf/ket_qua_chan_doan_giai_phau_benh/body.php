<div class="pdf-body" style="font-size: 13px;">
    <div style="text-align: center; color: #475443; font-size: 25px; font-weight: bold; margin-top: 10px;">KẾT QUẢ CHẨN ĐOÁN GIẢI PHẪU BỆNH</div>
    <div style="float: left; width: 70%">
        <div style="margin-left: 275px;">Ngày 24/02/2018</div>
    </div>
    <div style="float: left;">
        <div style="text-align: center; font-weight: bold;">Số lam GPB: 375/18</div>
    </div>
    <div style="clear: both;"></div>
    <div style="float: left; width: 50%;">
        <span style="font-weight: bold;">Họ tên: </span>
        <span style="font-weight: bold;">Nguyễn Văn A</span>
    </div>
    <div style="float: left; width: 20%">
        <span style="font-weight: bold;">Tuổi: 1971</span>
    </div>
    <div style="float: left;">
        <span style="font-weight: bold;">Para: </span>
    </div>
    <div style="clear: both;"></div>
    <div>
        <span style="font-weight: bold;">Địa chỉ: </span><span>123 Trần Hưng Đạo, Quận 5, TPHCM</span>
    </div>
    <div style="clear: both"></div>
    <div style="float: left; width: 70%">
        <span style="font-weight: bold;">Số nhập viện: </span>
        <span>18/003559</span>
    </div>
    <div style="float: left;">
        <span style="font-weight: bold;">Phòng: </span>
        <span>SOI</span>
    </div>
    <div style="clear: both"></div>
    <div>
        <span style="font-weight: bold;">Chẩn đoán lâm sàng: </span>
        <span>VẾT TRẮNG 6H, 12H/ LSIL/ HPV(+)</span>
    </div>
    <div style="float: left; width: 45%">
        <span style="font-weight: bold;">BS điều trị: </span>
        <span style="">NGUYỄN THỊ PHƯƠNG HẠNH</span>
    </div>
    <div style="float: left;">
        <span style="font-weight: bold;">Ngày mổ(sinh thiết): </span><span>10/02/2018</span>
        <span style="font-weight: bold;">Số Code: </span><span>PB10-028277</span>
    </div>
    <div style="clear: both"></div>

    <div style="font-weight: bold; text-align: center; font-size: 15px; margin-top: 10px;">
        PHẦN GIẢI PHẨU BỆNH
    </div>

    <div style="float: left; width: 20%; margin-top: 10px;">
        <div style="font-weight: bold;">I. Đại thể: </div>
    </div>
    <div style="float: left; ">
        <div>1 - 6H: MÔ KÍCH THƯỚC 0,3 CM</div>
        <div>2 - 12H: MÔ KÍCH THƯỚC 0,1 CM</div>
        <div>3- NẠO KÊNH: MÔ KÍCH THƯỚC 1 CM</div>
    </div>
    <div style="clear: both;"></div>
    <div style="margin-top: 10px;">
        <div style="font-weight: bold;">II. Vi thể: </div>
    </div>

    <div style="float: left; width: 50%;">
        <div><img class="" alt="" src="<?php echo base_url('assets/images/black.jpg'); ?>"></div>
    </div>
    <div style="float: left; ">
        <div><img class="" alt="" src="<?php echo base_url('assets/images/black.jpg'); ?>"></div>
    </div>
    <div style="clear: both;"></div>

    <div>
        <div>1,2 - Lớp thượng mô gai tăng sản với các tế bào có nhân to, tăng sắc, méo mó, có quầng sáng nhân, mô đệm thấm nhập viêm.</div>
        <div>3 - Chỉ có mô nhầy</div>
    </div>

    <div style="float: left; width: 35%;">
        <div style="font-weight: bold;">III. Chẩn đoán giải phẫu bệnh: </div>
    </div>
    <div style="float: left; ">
        <div>
            <div>1,2 - CONDYLOM CỔ TỬ CUNG.</div>
            <div>3 - CHỈ CÓ MÔ NHÂY.</div>
        </div>
    </div>
    <div style="clear: both;"></div>

    <div style=" margin-top: 20px;"><span style="font-weight: bold;">Đề nghị:</span></div>
    <div style="float: left;width: 40%;">&nbsp;</div>
    <div style="float: left">
        <div style="text-align: center; font-weight: bold;">Tp.HCM, Ngày 24 tháng 02 năm 2018</div>
        <div style="text-align: center; font-weight: bold;">BS GPB-TBH</div>
        <div style="text-align: center; font-weight: bold; margin-top: 80px;">THS.BS BÙI THỊ HỒNG KHANG</div>
    </div>
    <div style="clear: both;"></div>
</div>