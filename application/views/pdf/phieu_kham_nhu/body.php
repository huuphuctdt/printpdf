<div class="pdf-body" style="width: 100%;">
    <div style="width: 70%; float: left; text-align: center;">
        <h2 style="margin-top: 25px;">PHIẾU KHÁM NHŨ</h2>
    </div>
    <div style="float: left; text-align: center;">
        <?php if(isset($barcode) && $barcode != ""){ ?>
            <div><span style="font-weight: bold;">Mã số: </span> <?=$barcode;?></div>
        <?php } ?>
        <div><barcode code="<?=$barcode;?>" type="EAN128C" /></div>
    </div>
    <div style="clear: both;"></div>

    <div style="float: left; width: 40%; text-align: left;">
        <div style="font-weight: bold; font-size: 15px; padding-top: 5px;">I. HÀNH CHÍNH</div>
    </div>
    <div style="float: left; text-align: right;">
        <div style="font-style: italic; font-size: 15px; padding-top: 5px;">Thời gian khám lâm sàng cam kết trong khoảng 15 phút.</div>
    </div>
    <div style="clear: both;"></div>

    <div style="width: 40%; float: left; padding-top: 5px;">
        1. Họ và tên: Nguyễn Thị A
    </div>
    <div style="float: left;  padding-top: 5px;">
        <span>2. Sinh ngày: 07/12/1948</span>
        <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tuổi: 40</span>
    </div>
    <div style="clear: both;"></div>
    <div style="width: 40%; float: left; padding-top: 5px;">
        3. Giới tính: Nữ
    </div>
    <div style="float: left; padding-top: 5px;">
        4. Nghề nghiệp: Nhân viên
    </div>
    <div style="clear: both;"></div>
    <div style="width: 40%; float: left; padding-top: 5px;">
        5. Dân tộc: Kinh
    </div>
    <div style="float: left; padding-top: 5px;">
        6. Quốc tịch: Việt Nam
    </div>
    <div style="clear: both;"></div>
    <div style="padding-top: 5px;">
        <div style="display: inline-block;">
            7. Địa chỉ: 123 Trần Hưng Đạo, phường 13, Quận 1, TP.HCM
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            Điện thoại: 0352407555
        </div>
    </div>
    <div style="clear: both;"></div>
    <div style="padding-top: 5px;">
        8. Đến khám bệnh lúc: 08 giờ 51 phút, Ngày 25 tháng 05 năm 2018
    </div>
    <div style="font-weight: bold; font-size: 15px; padding-top: 5px;">II. HỎI BỆNH - THĂM KHÁM - ĐIỀU TRỊ</div>

    <div style="float: left; width: 80%; margin-top: 10px;">
        <span>- Tiền căn: </span>
        <span> KM > 20 năm khám đau vú (T)</span>
    </div>
    <div style="float: left;">
        <span>Para: </span>
        <span>1001</span>
    </div>
    <div style="clear: both;"></div>

    <div style="margin-top: 10px;"><span>- Bệnh lý: </span></div>

    <div style="float: left; width: 20%; margin-top: 10px;">
        <span>- Thăm khám:</span>
    </div>
    <div style="float: left;">
        <div>khám vú</div>
        <div>P: mềm không u</div>
        <div>T: mềm không u</div>
        <div>SA vú: BT</div>
    </div>
    <div style="clear: both;"></div>

    <div style="margin-top: 10px;">
        <span>- Chẩn đoán: </span>
        <span>Vú BT (N64.9)</span>
    </div>

    <div style="margin-top: 10px;">
        <span>- Theo dõi điều trị: </span>
        <span>TK 3 tháng</span>
    </div>

    <div style="margin-top: 10px;">
        <span>- Ngày hẹn tái khám: </span>
        <span>25/08/2018</span>
    </div>


    <div style="float: left;width: 60%;">&nbsp;</div>
    <div style="float: left">
        <div style="text-align: center; font-weight: bold;">Ngày 25 tháng 05 năm 2017</div>
        <div style="text-align: center; font-weight: bold;">BÁC SĨ KHÁM BỆNH</div>
        <div style="text-align: center; font-weight: bold; margin-top: 80px;">NGUYỄN THỊ THU HỒNG</div>
    </div>
    <div style="clear: both;"></div>
</div>