<div class="pdf-body">
    <div style="color:#97A5BF;font-weight: bold; font-size: 25px; text-align: center;">KẾT QUẢ SIÊU ÂM</div>
    <div style="text-align: center;">Ngày: 28/05/2018 07:45:59</div>
    <div style="text-align: right;">
        <div><barcode code="<?=$barcode;?>" type="EAN128C" /></div>
        <?php if(isset($barcode) && $barcode != ""){ ?>
            <div><span style="font-weight: bold;">Mã số (Code): </span> <?=$barcode;?></div>
        <?php } ?>
    </div>
    <hr/>
    <div>
        <span style="font-weight: bold;">Mã BN: </span> <span>18/004338</span>
    </div>
    <div style="float: left; width: 60%;">
        <span style="font-weight: bold;">Họ tên: </span> <span>CHU THỊ THỦY NGỌC</span>
    </div>
    <div style="float: left">
        <span style="font-weight: bold;">Phòng: </span> <span>Phòng SÂ số 8 (3D,4D)</span>
    </div>
    <div style="clear: both;"></div>
    <div style="float: left; width: 60%;">
        <span style="font-weight: bold;">Địa chỉ: </span>
        <span>13 K1 KHU DÂN CƯ TÂN QUY ĐÔNG, PHƯỜNG TÂN PHONG, QUẬN 7, TPHCM</span>
    </div>
    <div style="float: left">
        <span style="font-weight: bold;">Năm sinh: </span> <span>1989</span>
    </div>
    <div style="clear: both;"></div>
    <div>
        <span style="font-weight: bold;">Chẩn đoán lâm sàng: </span>
    </div>
    <div style="float: left; width: 60%;">
        <span style="font-weight: bold;">Bác sĩ chỉ định: </span><span style="font-weight: bold;"> ĐẶNG THỊ TUYẾT NGA</span>
    </div>
    <div style="float: left">
        <span style="font-weight: bold;">BS/NHS: </span><span style="">TRẦN THỊ THẢO</span>
    </div>
    <div style="clear: both;"></div>
    <hr/>
    <div style="color:#97A5BF;font-weight: bold; font-size: 20px; text-align: center;">
        <div style="font-weight: bold; text-align: center;">HÌNH ẢNH SIÊU ÂM</div>
    </div>
    <div style="float: left; width: 50%; text-align: center; margin-top: 10px;">
        <div><img height="215" width="290"  class="" alt="" src="<?php echo base_url('assets/images/black.jpg'); ?>"></div>
    </div>
    <div style="float: left; text-align: center;">
        <div><img height="215" width="290"  class="" alt="" src="<?php echo base_url('assets/images/black.jpg'); ?>"></div>
    </div>
    <div style="clear: both;"></div>
    <div style="float: left; width: 50%; text-align: center; margin-top: 10px;">
        <div><img height="215" width="290"  class="" alt="" src="<?php echo base_url('assets/images/black.jpg'); ?>"></div>
    </div>
    <div style="float: left; text-align: center;">
        <div><img height="215" width="290"  class="" alt="" src="<?php echo base_url('assets/images/black.jpg'); ?>"></div>
    </div>
    <div style="clear: both;"></div>
    <div style="float: left; width: 50%; text-align: center; margin-top: 10px;">
        <div><img height="215" width="290"  class="" alt="" src="<?php echo base_url('assets/images/black.jpg'); ?>"></div>
    </div>
    <div style="float: left; text-align: center;">
        <div><img height="215" width="290"  class="" alt="" src="<?php echo base_url('assets/images/black.jpg'); ?>"></div>
    </div>
    <div style="clear: both;"></div>
</div>
<pagebreak>
<div class="pdf-body" style="font-size: 13px;">
    <div style="color:#97A5BF;font-weight: bold; font-size: 25px; text-align: center;">KẾT QUẢ SIÊU ÂM</div>
    <div style="text-align: center;">Ngày: 28/05/2018 07:45:59</div>
    <div style="text-align: right;">
        <div><barcode code="<?=$barcode;?>" type="EAN128C" /></div>
        <?php if(isset($barcode) && $barcode != ""){ ?>
            <div><span style="font-weight: bold;">Mã số (Code): </span> <?=$barcode;?></div>
        <?php } ?>
    </div>
    <hr/>
    <div>
        <span style="font-weight: bold;">Mã BN: </span> <span>18/004338</span>
    </div>
    <div style="float: left; width: 60%;">
        <span style="font-weight: bold;">Họ tên: </span> <span>CHU THỊ THỦY NGỌC</span>
    </div>
    <div style="float: left">
        <span style="font-weight: bold;">Phòng: </span> <span>Phòng SÂ số 8 (3D,4D)</span>
    </div>
    <div style="clear: both;"></div>
    <div style="float: left; width: 60%;">
        <span style="font-weight: bold;">Địa chỉ: </span>
        <span>13 K1 KHU DÂN CƯ TÂN QUY ĐÔNG, PHƯỜNG TÂN PHONG, QUẬN 7, TPHCM</span>
    </div>
    <div style="float: left">
        <span style="font-weight: bold;">Năm sinh: </span> <span>1989</span>
    </div>
    <div style="clear: both;"></div>
    <div>
        <span style="font-weight: bold;">Chẩn đoán lâm sàng: </span>
    </div>
    <div style="float: left; width: 60%;">
        <span style="font-weight: bold;">Bác sĩ chỉ định: </span><span style="font-weight: bold;"> ĐẶNG THỊ TUYẾT NGA</span>
    </div>
    <div style="float: left">
        <span style="font-weight: bold;">BS/NHS: </span><span style="">TRẦN THỊ THẢO</span>
    </div>
    <div style="clear: both;"></div>
    <hr/>
    <div>
        <div style="color:#97A5BF;font-weight: bold; font-size: 15px; text-align: left;">Siêu âm 4D</div>
    </div>
    <div>
        <span>Số lượng thai : 1</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <span>Ngôi thai : </span>
    </div>
    <div>
        <span>Tim thai đều : (+),</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <span>nhịp/phút</span>
    </div>
    <div>
        <span>ĐKLĐ : </span>&nbsp;&nbsp;&nbsp;&nbsp;<span>mm</span>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <span>CVVĐ : </span>&nbsp;&nbsp;&nbsp;&nbsp;<span>cm</span>
    </div>
    <div>
        <span>CDXĐ : </span>&nbsp;&nbsp;&nbsp;&nbsp;<span>mm</span>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <span>CDX cánh tay : </span>&nbsp;&nbsp;&nbsp;&nbsp;<span>mm</span>
    </div>
    <div>
        <span>ĐKNB : </span>&nbsp;&nbsp;&nbsp;&nbsp;<span>mm</span>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <span>CVVB : </span>&nbsp;&nbsp;&nbsp;&nbsp;<span>mm</span>
    </div>
    <div>
        <span>ULCN : </span>&nbsp;&nbsp;&nbsp;&nbsp;<span>gram</span>
    </div>
    <div>
        <span>ĐK ngang tiểu não : </span>&nbsp;&nbsp;&nbsp;&nbsp;<span>mm</span>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <span>Hố sau : </span>&nbsp;&nbsp;&nbsp;&nbsp;<span>mm</span>
    </div>
    <div>
        <span>Ngã tư não thất : </span>&nbsp;&nbsp;&nbsp;&nbsp;<span>mm ( Dãn > 10 mm)</span>
    </div>
    <div>
        <span>ĐK gian 02 hốc mắt : </span>&nbsp;&nbsp;&nbsp;&nbsp;<span>mm</span>
    </div>
    <div>
        <span>CDXM : </span>&nbsp;&nbsp;&nbsp;&nbsp;<span>mm</span>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <span>NF : </span>&nbsp;&nbsp;&nbsp;&nbsp;<span>mm ( < 24 tuần )</span>
    </div>
    <div>
        <span>Lượng nước ối : </span><span>bình thường</span>
    </div>
    <div>
        <span>Vị trí nhau bám : </span><span>mặt nước, nhóm I</span>
    </div>
    <div>
        <span>Độ trưởng thành : </span><span>0</span>
    </div>
    <div>
        <span>Dự sanh : </span>
    </div>
    <div>
        <div>1. Đầu mặt cổ :</div>
        <div>- Vòm sọ liên tục và hinh dạng bình thường</div>
        <div>- Hai bán cầu đại não cân đối, đồi thị bình thường, có vách trong suốt, hệ thống não thất
        không dãn, đám rối mạch mạc bên 2 bên không có nang</div>
        <div>- Tiểu não tương ứng với tuổi thai, có thấy thùy nhộng</div>
        <div>- Không thất sứt môi</div>
        <div>- Cột sống không gập góc</div>
        <div>2. Lồng ngực :</div>
        <div>- Tim: 2 tâm nhĩ đều nhau, 2 tâm thất đều nhau, quan sát thấy dấu bắt chéo 2 đại dộng mạch,
        không có tràn dịch ngoài tim.</div>
        <div>- Phổi: mật độ echo đồng nhất, không tràn dịch màn phổi</div>
        <div>- Không thấy dấu hiệu thoát vị hoành</div>
        <div>3. Bụng : </div>
        <div>- Thành bụng đóng kín, không thất thoát vị rốn</div>
        <div>- Có túi dịch da dày ở vị trí bình thường</div>
        <div>- Gan echo dày động dạng, túi mật bình thường, đường mật trong gan không dãn</div>
        <div>- Các quai ruột không dãn</div>
        <div>- Hai thận thấy ở vị trí bình thường, bể thận không dãn</div>
        <div>- Có bàng quang</div>
        <div>- Bụng không có dịch tự do</div>
        <div>4. Tứ chi : Tay chân không khoèo, có cử động tốt.</div>
        <div>* Mẹ : + Buồng trứng ( P ): không u</div>
        <div>+ Buồng trúng (T): không u</div>
        <div>Siêu âm chỉ là một khám nghiệm cận lâm sàng, siêu âm không thể phát hiện tất cả các dị tật thai nhi
            (vui lòng gặp tư vấn với bác sĩ chỉ định).</div>
    </div>
</div>
<pagebreak>
<div class="pdf-body" style="font-size: 13px;">
    <div style="color:#97A5BF;font-weight: bold; font-size: 25px; text-align: center;">KẾT QUẢ SIÊU ÂM</div>
    <div style="text-align: center;">Ngày: 28/05/2018 07:45:59</div>
    <div style="text-align: right;">
        <div><barcode code="<?=$barcode;?>" type="EAN128C" /></div>
        <?php if(isset($barcode) && $barcode != ""){ ?>
            <div><span style="font-weight: bold;">Mã số (Code): </span> <?=$barcode;?></div>
        <?php } ?>
    </div>
    <hr/>
    <div>
        <span style="font-weight: bold;">Mã BN: </span> <span>18/004338</span>
    </div>
    <div style="float: left; width: 60%;">
        <span style="font-weight: bold;">Họ tên: </span> <span>CHU THỊ THỦY NGỌC</span>
    </div>
    <div style="float: left">
        <span style="font-weight: bold;">Phòng: </span> <span>Phòng SÂ số 8 (3D,4D)</span>
    </div>
    <div style="clear: both;"></div>
    <div style="float: left; width: 60%;">
        <span style="font-weight: bold;">Địa chỉ: </span>
        <span>13 K1 KHU DÂN CƯ TÂN QUY ĐÔNG, PHƯỜNG TÂN PHONG, QUẬN 7, TPHCM</span>
    </div>
    <div style="float: left">
        <span style="font-weight: bold;">Năm sinh: </span> <span>1989</span>
    </div>
    <div style="clear: both;"></div>
    <div>
        <span style="font-weight: bold;">Chẩn đoán lâm sàng: </span>
    </div>
    <div style="float: left; width: 60%;">
        <span style="font-weight: bold;">Bác sĩ chỉ định: </span><span style="font-weight: bold;"> ĐẶNG THỊ TUYẾT NGA</span>
    </div>
    <div style="float: left">
        <span style="font-weight: bold;">BS/NHS: </span><span style="">TRẦN THỊ THẢO</span>
    </div>
    <div style="clear: both;"></div>
    <hr/>
    <div>
        <span style="color:#97A5BF; font-weight: bold;">Kết luận : </span>
        <span>
            01 THAI SÔNG TRONG TỬ CUNG KHOẢNG &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TUẦN &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; NGÀY
            (THEO DỰ SANH) CÁC SỐ ĐO SINH HỌC NẰM Ở BÁCH PHÂN VỊ TỪ&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ĐẾN&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            SO VỚI TUỔI THAI.
        </span>
    </div>
    <div>
        <span style="color:#97A5BF; font-weight: bold;">Đề nghị : </span>
    </div>

    <div style="float: left;width: 60%;">&nbsp;</div>
    <div style="float: left">
        <div style="text-align: center; font-weight: bold;">
            Ngày:28 tháng 05 năm 2018
        </div>
        <div style="text-align: center; font-weight: bold;">
            Bác sĩ Siêu âm
        </div>
        <div style="text-align: center; font-weight: bold; margin-top: 80px;">
            BS. PHẠM MẠNH HÙNG
        </div>
    </div>
    <div style="clear: both;"></div>
</div>