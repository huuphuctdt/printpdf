<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<div class="pdf-header">
    <div>
        <div class="header-logo" style="float: left; width: 15%;">
            <img class="svg-target" alt="alt-text" src="<?php echo base_url('assets/images/logo.png'); ?>">
        </div>
        <div class="header-title" style="margin: auto;width: 75%;padding: 10px; text-align: center;">
            <div style="font-size: 15px; padding: 2px;font-weight: bold;">BỆNH VIỆN PHỤ SẢN QUỐC TẾ SÀI GÒN</div>
            <div style="font-size: 15px; padding: 2px;color: #ff0000; font-style: italic;font-weight: bold;">SAIGON INTERNATIONAL OB-GYN. HOSPITAL</div>
            <div style="font-size: 15px; padding: 2px;font-weight: bold;">63 Bùi Thị Xuân, Phường Phạm Ngũ Lão, Quận 1, TP. Hồ Chí Minh</div>
            <div style="font-size: 15px; padding: 2px;font-weight: bold;">ĐT: 0899909268 - 70 (3 lines)  &nbsp;&nbsp;&nbsp;Fax: (84.4) 3925 3184</div>
        </div>
        <div style="clear: both;"></div>
    </div>
</div>