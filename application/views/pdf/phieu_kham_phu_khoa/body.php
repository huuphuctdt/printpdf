<div class="pdf-body" style="width: 100%;">
    <div style="width: 70%; float: left; text-align: center;">
        <h2 style="margin-top: 25px;">PHIẾU KHÁM PHỤ KHOA</h2>
    </div>
    <div style="float: left; text-align: center;">
        <?php if(isset($barcode) && $barcode != ""){ ?>
            <div><span style="font-weight: bold;">Mã số: </span> <?=$barcode;?></div>
        <?php } ?>
        <div><barcode code="<?=$barcode;?>" type="EAN128C" /></div>
    </div>
    <div style="clear: both;"></div>
    <div style="font-weight: bold; font-size: 15px; padding-top: 5px;">I. HÀNH CHÍNH</div>
    <div style="width: 40%; float: left; padding-top: 5px;">
        1. Họ và tên: Nguyễn Thị A
    </div>
    <div style="float: left;  padding-top: 5px;">
        <span>2. Sinh ngày: 07/12/1970</span>
        <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tuổi: 48</span>
    </div>
    <div style="clear: both;"></div>
    <div style="width: 40%; float: left; padding-top: 5px;">
        3. Giới tính: Nữ
    </div>
    <div style="float: left; padding-top: 5px;">
        4. Nghề nghiệp: Nhân viên
    </div>
    <div style="clear: both;"></div>
    <div style="width: 40%; float: left; padding-top: 5px;">
        5. Dân tộc: Kinh
    </div>
    <div style="float: left; padding-top: 5px;">
        6. Quốc tịch: Việt Nam
    </div>
    <div style="clear: both;"></div>
    <div style="padding-top: 5px;">
        <div style="display: inline-block;">
            7. Địa chỉ: 123 Trần Hưng Đạo, phường 13, Quận 1, TP.HCM
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            Điện thoại: 0352407555
        </div>
    </div>
    <div style="clear: both;"></div>
    <div style="padding-top: 5px;">
        8. Đến khám bệnh lúc: 0 giờ 0 phút, Ngày 19 tháng 1 năm 2017
    </div>
    <div style="font-weight: bold; font-size: 15px; padding-top: 5px;">II. HỎI BỆNH - THĂM KHÁM - ĐIỀU TRỊ</div>
    <div style="width: 50%; float: left; padding-top: 5px;">
        - Lý do vào viện:
    </div>
    <div style="float: left; padding-top: 5px;">
        - Kinh cuối:
    </div>
    <div style="padding-top: 5px;">
        <div>- Thăm khám: + Khám</div>
        <div style="padding-left: 100px; margin-top: 10px;">
            <div>-Âm đạo: ít huyến trắng</div>
            <div>-Cổ tử cung: láng</div>
            <div>-Tử cuing: BT</div>
            <div>-2 phần phụ: BT</div>
            <div>-SA: </div>
            <div>-Pap: </div>
            <div>-Soi CTC: </div>
            <div>-SN: </div>
        </div>
        <div style="padding-top: 5px;">- Chẩn đoán: VIÊM AM ĐẠP TẠP TRÙNG / LẠC TUYẾN (N76.0)</div>
        <div style="padding-top: 5px;">- Theo dõi điều trị: Safaria (30v)</div>
        <div style="padding-top: 5px;">- Ngày hẹn tái khám: </div>
    </div>

    <div style="float: left;width: 60%;">&nbsp;</div>
    <div style="float: left">
        <div style="text-align: center; font-weight: bold;">Ngày:19 tháng 01 năm 2017</div>
        <div style="text-align: center; font-weight: bold;">Bác sĩ khám bệnh</div>
        <div style="text-align: center; font-weight: bold; margin-top: 80px;">ĐẶNG THỊ TUYẾT NGA</div>
    </div>
    <div style="clear: both;"></div>
</div>