<div class="pdf-body">
    <div style="font-weight: bold; font-size: 25px; text-align: center;">KẾT QUẢ SIÊU ÂM</div>
    <div style="text-align: center;">Ngày: 28/05/2018 07:45:59</div>
    <div style="text-align: right;">
        <div><barcode code="<?=$barcode;?>" type="EAN128C" /></div>
        <?php if(isset($barcode) && $barcode != ""){ ?>
            <div><span style="font-weight: bold;">Mã số (Code): </span> <?=$barcode;?></div>
        <?php } ?>
    </div>
    <hr/>
    <table>
        <tr>
            <td colspan="2" style="padding: 5px;"><span style="font-weight: bold;">Mã KN: </span> <span>18/012937</span></td>
        </tr>
        <tr>
            <td style="padding: 5px;"><span style="font-weight: bold;">Họ tên: </span> <span>CHU THỊ THỦY NGỌC</span></td>
            <td style="padding: 5px;"><span style="font-weight: bold;">Phòng: </span> <span>Phòng siêu âm số 6</span></td>
        </tr>
        <tr>
            <td style="padding: 5px;">
                <span style="font-weight: bold;">Địa chỉ: </span>
                <span>13 K1 KHU DÂN CƯ TÂN QUY ĐÔNG, PHƯỜNG TÂN PHONG, QUẬN 7, TPH&nbsp;&nbsp;&nbsp;cm</span>
            </td>
            <td style="padding: 5px;"><span style="font-weight: bold;">Năm sinh: </span> <span>1990</span></td>
        </tr>
        <tr>
            <td colspan="2" style="padding: 5px;"><span style="font-weight: bold;">Chẩn đoán lâm sàng: </span> <span>1983</span></td>
        </tr>
        <tr>
            <td style="padding: 5px;"><span style="font-weight: bold;">Bác sĩ chỉ định: </span><span style="font-weight: bold;"> DƯƠNG PHƯƠNG MAI</span></td>
            <td style="padding: 5px;"><span style="font-weight: bold;">BS/NHS: </span><span style="">PHAN THỊ TƯƠI</span></td>
        </tr>

    </table>
    <hr/>
    <div style="width: 60%; float:left;">
        <div style="font-weight: bold; text-align: center;">SIÊU ÂM THAI</div>
    </div>
    <div style="float:left;">
        <div style="text-align: center;">HÌNH ẢNH</div>
    </div>
    <div style="clear: both;"></div>
    <div style="width: 60%; float: left;">
        <div style="padding-top: 5px;">THAI</div>
        <div style="padding-top: 5px;">
            <span>- Số thai : 01</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span>Ngôi : </span>
        </div>
        <div style="padding-top: 5px;">
            <span>- Tim thai : có, nhịp tim 1/p.</span>
        </div>
        <div style="padding-top: 5px;">
            <span>- ĐKLĐ (BPD) : &nbsp;&nbsp;&nbsp;mm</span>&nbsp;&nbsp;&nbsp;<span>- CVVĐ (HC) : &nbsp;&nbsp;&nbsp;cm</span>
        </div>
        <div style="padding-top: 5px;">
            <span>- CDXĐ (FL) : &nbsp;&nbsp;&nbsp;mm</span>
        </div>
        <div style="padding-top: 5px;">
            <span>- ĐKNB (TAD) : &nbsp;&nbsp;&nbsp;mm</span>&nbsp;&nbsp;&nbsp;<span>- CVVB (AC) : &nbsp;&nbsp;&nbsp;cm</span>
        </div>
        <div style="padding-top: 5px;">
            <span>- ƯLCN : gram</span>
        </div>
        <div style="padding-left: 60px;">
            <span>NHAU:</span>
        </div>
        <div style="padding-top: 5px;">
            <span>- Vị trí : </span>
        </div>
        <div style="padding-top: 5px;">
            <span>- Độ trưởng thành : </span>
        </div>
        <div style="padding-left: 60px;">
            <span>NƯỚC ỐI : Bình thường</span>
        </div>
        <div style="padding-left: 120px;">
            <span>Chỉ số AFI = &nbsp;&nbsp;&nbsp;cm</span>
        </div>
        <div style="padding-top: 5px;">
            <span>- Bất thường thai : Hiện chưa phát hiện</span>
        </div>
        <div style="padding-top: 5px;">
            <div>* Mẹ : + Bường trứng (P) : Không u.</div>
            <div style="padding-left: 45px;">+ Bường trứng (T) : Không u.</div>
        </div>
        <div style="padding-top: 5px;">
            Siêu âm chỉ là một khám nghiệm cận lâm sàng, siêu âm không thể
            phát hiện tất cả các dị tật thai nhi (vui lòng gặp tư vấn với bác sĩ chỉ định)
        </div>
    </div>
    <div style="float: left;">
        <div style="margin-top: 100px;"><img class="" alt="" src="<?php echo base_url('assets/images/black.jpg'); ?>"></div>
    </div>
    <div style="padding-top: 15px;">
        <span style="font-weight: bold;">Kết luận : </span>
        <span>1 THAI SỐNG TRONG TỬ CUNG KHOẢNG</span>&nbsp;&nbsp;&nbsp;&nbsp;<span>TUẦN</span>
    </div>
    <div style="padding-top: 5px;">
        <span style="font-weight: bold;">Đề nghị : </span>
    </div>
    <div></div>

    <div style="float: left;width: 60%;">&nbsp;</div>
    <div style="float: left">
        <div style="text-align: center; font-weight: bold;">Tp.HCM, Ngày 28 tháng 5 năm 2018</div>
        <div style="text-align: center; font-weight: bold;">Bác sĩ Siêu âm</div>
        <div style="text-align: center; font-weight: bold; margin-top: 80px;">BS. DƯƠNG THỊ PHƯƠNG THẢO</div>
    </div>
    <div style="clear: both;"></div>
</div>