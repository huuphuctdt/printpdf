<div class="pdf-body" style="font-size: 13px;">
    <div style="text-align: center; color: #475443; font-size: 25px; font-weight: bold; margin-top: 10px;">KẾT QUẢ CHỌC HÚT TẾ BÀO BẰNG KIM NHỎ</div>
    <div style="float: left; width: 70%">
        <div style="margin-left: 240px; font-weight: bold;">Fine needle aspiration(FNA)</div>
        <div style="margin-left: 275px;">Ngày 30/05/2018</div>
    </div>
    <div style="float: left;">
        <div style="text-align: center; font-weight: bold;">Số lam FNA: 19/08</div>
    </div>
    <div style="clear: both;"></div>
    <div style="float: left; width: 50%;">
        <span style="font-weight: bold;">Họ tên: </span>
        <span style="font-weight: bold;">Nguyễn Văn A</span>
    </div>
    <div style="float: left; width: 20%">
        <span style="font-weight: bold;">Tuổi: 1971</span>
    </div>
    <div style="float: left;">
        <span style="font-weight: bold;">Số nhập viện: 18/014330</span>
    </div>
    <div style="clear: both;"></div>
    <div style="float: left;width: 70%;">
        <span style="font-weight: bold;">Địa chỉ: </span><span>123 Trần Hưng Đạo, Quận 5, TPHCM</span>
    </div>
    <div style="float: left;">
        <span style="font-weight: bold;">Số ĐT: </span><span>0123456789</span>
    </div>
    <div style="clear: both"></div>
    <div style="float: left; width: 70%">
        <span style="font-weight: bold;">Tiểu sử: </span>
    </div>
    <div style="clear: both"></div>
    <div style="float: left; width: 40%">
        <span style="font-weight: bold;">BS điều trị: </span>
        <span style="font-weight: bold;">TRẦN ANH TUẤN</span>
    </div>
    <div style="float: left;">
        <span style="font-weight: bold;">Ngày mổ(sinh thiết): </span><span>21/03/2018</span>
        <span style="font-weight: bold;">Số Code: </span><span>KN10-002876</span>
    </div>
    <div style="clear: both"></div>

    <div style="font-weight: bold; font-size: 15px;">I. Lâm sàng:</div>
    <div style="float: left; width: 60%; padding-top: 45px;">
        - 1/4 trên ngoài vú (T) có khối 1 cm, giới hạn rõ, di động.
    </div>
    <div style="float: left; text-align: right;">
        <div><img height="100" width="200"  class="" alt="" src="<?php echo base_url('assets/images/black.jpg'); ?>"></div>
    </div>
    <div style="clear: both;"></div>
    <div style="font-weight: bold; font-size: 15px; margin-top: 20px;">II. Nhận xét lúc lấy mẫu thử:</div>
    <div style="margin-left: 50px;">FNA vú (T), phết 2 lam.</div>
    <div style="font-weight: bold; font-size: 15px;  margin-top: 20px;">III. Vi thể:</div>
    <div style="float: left; width: 50%;">
        <div><img class="" alt="" src="<?php echo base_url('assets/images/black.jpg'); ?>"></div>
    </div>
    <div style="float: left;">
        <div><img class="" alt="" src="<?php echo base_url('assets/images/black.jpg'); ?>"></div>
    </div>
    <div style="clear: both;"></div>
    <div>Hiện diện các đám tế bào biểu mô và cơ biểu mô tuyến vú tăng sản lành tính trên nên mô đệm dạng niêm</div>
    <div style="font-weight: bold; font-size: 15px;  margin-top: 20px;">
        <span>IV. Chẩn đoán:</span>
        <span>BƯỚU SỢI TUYẾN TUYẾN VÚ.</span>
    </div>

    <div style=" margin-top: 20px;"><span style="font-weight: bold;">Đề nghị:</span></div>
    <div style="float: left;width: 40%;">&nbsp;</div>
    <div style="float: left">
        <div style="text-align: center; font-weight: bold;">Tp.HCM, Ngày 30 tháng 5 năm 2018</div>
        <div style="text-align: center; font-weight: bold;">BS GPB-TBH</div>
        <div style="text-align: center; font-weight: bold; margin-top: 80px;">DR, BUI NGOC DE</div>
    </div>
    <div style="clear: both;"></div>
</div>