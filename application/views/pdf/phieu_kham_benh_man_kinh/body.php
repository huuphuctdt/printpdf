<div class="pdf-body" style="width: 100%;">
    <div style="width: 70%; float: left; text-align: center;">
        <h2 style="margin-top: 25px;">PHIẾU KHÁM BỆNH MÃN KINH</h2>
    </div>
    <div style="float: left; text-align: center;">
        <?php if(isset($barcode) && $barcode != ""){ ?>
            <div><span style="font-weight: bold;">Mã số: </span> <?=$barcode;?></div>
        <?php } ?>
        <div><barcode code="<?=$barcode;?>" type="EAN128C" /></div>
    </div>
    <div style="clear: both;"></div>

    <div style="float: left; width: 40%; text-align: left;">
        <div style="font-weight: bold; font-size: 15px; padding-top: 5px;">I. HÀNH CHÍNH</div>
    </div>
    <div style="float: left; text-align: right;">
        <div style="font-style: italic; font-size: 15px; padding-top: 5px;">Thời gian khám lâm sàng cam kết trong khoảng 15 phút.</div>
    </div>
    <div style="clear: both;"></div>

    <div style="width: 40%; float: left; padding-top: 5px;">
        1. Họ và tên: Nguyễn Thị A
    </div>
    <div style="float: left;  padding-top: 5px;">
        <span>2. Sinh ngày: 07/12/1970</span>
        <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tuổi: 48</span>
    </div>
    <div style="clear: both;"></div>
    <div style="width: 40%; float: left; padding-top: 5px;">
        3. Giới tính: Nữ
    </div>
    <div style="float: left; padding-top: 5px;">
        4. Nghề nghiệp: Nội trợ
    </div>
    <div style="clear: both;"></div>
    <div style="width: 40%; float: left; padding-top: 5px;">
        5. Dân tộc: Kinh
    </div>
    <div style="float: left; padding-top: 5px;">
        6. Quốc tịch: Việt Nam
    </div>
    <div style="clear: both;"></div>
    <div style="padding-top: 5px;">
        <div style="display: inline-block;">
            7. Địa chỉ: 123 Trần Hưng Đạo, phường 13, Quận 1, TP.HCM
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            Điện thoại: 0352407555
        </div>
    </div>
    <div style="clear: both;"></div>
    <div style="padding-top: 5px;">
        8. Đến khám bệnh lúc: 07 giờ 48 phút, Ngày 04 tháng 05 năm 2018
    </div>
    <div style="font-weight: bold; font-size: 15px; padding-top: 5px;">II. HỎI BỆNH - THĂM KHÁM - ĐIỀU TRỊ</div>


    <div style="float: left; width: 50%; margin-top: 10px;">
        - Ngày ra kinh: 14/05/2018
    </div>
    <div style="float: left;">
        - Ngày sạch kinh: 14/05/2018
    </div>
    <div style="clear: both;"></div>

    <div style="float: left; width: 50%; margin-top: 10px;">
        - Số lần tiểu ngày:
    </div>
    <div style="float: left;">
        - Số lần giao hợp/tháng:
    </div>
    <div style="clear: both;"></div>

    <div style=" margin-top: 10px;">
        - Khám và điều trị: Mãn kinh 3 năm, Para 3003AĐ ít HT, CTC láng, có dây vòng TC, 2pp BT
    </div>

    <div style=" margin-top: 10px;">
        - Chẩn đoán - điều trị: Lấy vòng tránh thai (Z30.5)
    </div>

    <div style="margin-top: 10px;">
        - Ngày hẹn tái khám: 14/11/2018
    </div>
    <div style="clear: both;"></div>

    <div style="float: left;width: 60%;">&nbsp;</div>
    <div style="float: left">
        <div style="text-align: center; font-weight: bold;">Ngày:04 tháng 05 năm 2018</div>
        <div style="text-align: center; font-weight: bold;">BÁC SĨ KHÁM BỆNH</div>
        <div style="text-align: center; font-weight: bold; margin-top: 80px;">TRƯƠNG THỊ THANH NGUYỆT</div>
    </div>
    <div style="clear: both;"></div>
</div>