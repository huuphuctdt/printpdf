<div class="pdf-body" style="width: 100%;">
    <div style="width: 70%; float: left; text-align: center;">
        <h2 style="margin-top: 25px;">PHIẾU KHÁM THAI</h2>
    </div>
    <div style="float: left; text-align: center;">
        <?php if(isset($barcode) && $barcode != ""){ ?>
            <div><span style="font-weight: bold;">Mã số: </span> <?=$barcode;?></div>
        <?php } ?>
        <div><barcode code="<?=$barcode;?>" type="EAN128C" /></div>
    </div>
    <div style="clear: both;"></div>

    <div style="float: left; width: 40%; text-align: left;">
        <div style="font-weight: bold; font-size: 15px; padding-top: 5px;">I. HÀNH CHÍNH</div>
    </div>
    <div style="float: left; text-align: right;">
        <div style="font-style: italic; font-size: 15px; padding-top: 5px;">Thời gian khám lâm sàng cam kết trong khoảng 15 phút.</div>
    </div>
    <div style="clear: both;"></div>

    <div style="width: 40%; float: left; padding-top: 5px;">
        1. Họ và tên: Nguyễn Thị A
    </div>
    <div style="float: left;  padding-top: 5px;">
        <span>2. Sinh ngày: 07/12/1970</span>
        <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tuổi: 48</span>
    </div>
    <div style="clear: both;"></div>
    <div style="width: 40%; float: left; padding-top: 5px;">
        3. Giới tính: Nữ
    </div>
    <div style="float: left; padding-top: 5px;">
        4. Nghề nghiệp: Nhân viên
    </div>
    <div style="clear: both;"></div>
    <div style="width: 40%; float: left; padding-top: 5px;">
        5. Dân tộc: Kinh
    </div>
    <div style="float: left; padding-top: 5px;">
        6. Quốc tịch: Việt Nam
    </div>
    <div style="clear: both;"></div>
    <div style="padding-top: 5px;">
        <div style="display: inline-block;">
            7. Địa chỉ: 123 Trần Hưng Đạo, phường 13, Quận 1, TP.HCM
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            Điện thoại: 0352407555
        </div>
    </div>
    <div style="clear: both;"></div>
    <div style="padding-top: 5px;">
        8. Đến khám bệnh lúc: 13 giờ 33 phút, Ngày 04 tháng 05 năm 2018
    </div>
    <div style="font-weight: bold; font-size: 15px; padding-top: 5px;">II. HỎI BỆNH - THĂM KHÁM - ĐIỀU TRỊ</div>

    <div style="float: left;width: 20%; margin-top: 10px;">
        - Số lần sanh(lần):
    </div>
    <div style="float: left;width: 20%;">
        2022 ST2, 3400GR, 2 SẤY
    </div>
    <div style="float: left;width: 33%;">
        - Ngày dự sanh: 30/06/2018
    </div>
    <div style="float: left;">
        - Kinh cuối: 16/09/2017
    </div>
    <div style="clear: both;"></div>
    <div style="margin-top: 10px;">- Bệnh sử: tái khám 3 tuần</div>
    <div style="clear: both;"></div>
    <div style="float: left;width: 25%;  margin-top: 10px;">
        - Cân nặng(kg): 56
    </div>
    <div style="float: left;width: 25%;">
        - Chiều cao(cm): 156
    </div>
    <div style="float: left;width: 15%;">
        - BMI: 23.01
    </div>
    <div style="float: left; text-align: right;">
        - Huyết áp(mmHg): 10/06
    </div>
    <div style="clear: both;"></div>
    <div style="float: left; width: 50%; margin-top: 10px;">
        - Chiều cao tử cung:
    </div>
    <div style="float: left;">
        - Tim thai:
    </div>
    <div style="clear: both;"></div>
    <div style="float: left; width: 30%;">
        - Thăm khám:
    </div>
    <div style="float: left">
        <div>+ Khám</div>
        <div style="margin-left: 10px;">
            - Âm đạo: Sạch<br>
            - Cổ tử cung: đóng<br>
            - Tử cung: BT<br>
            - 2 phần phụ: BT<br>
            - SA: 1 thai sống ngôi đầu
        </div>
    </div>
    <div style="margin-top: 10px;">
        - Chẩn đoán: THAI 32 TUẦN 1 NGÀY (Z34)
    </div>
    <div style="margin-top: 10px;">
        - Theo dõi điều trị: Hemopoly 5ml (40v); Kiddiecal (60v)
    </div>
    <div style="margin-top: 10px;">
        - Ngày hẹn tái khám: 25/05/2018
    </div>
    <div style="clear: both;"></div>

    <div style="float: left;width: 60%;">&nbsp;</div>
    <div style="float: left">
        <div style="text-align: center; font-weight: bold;">Ngày:04 tháng 05 năm 2018</div>
        <div style="text-align: center; font-weight: bold;">BÁC SĨ KHÁM BỆNH</div>
        <div style="text-align: center; font-weight: bold; margin-top: 80px;">NGUYỄN THỊ THU HỒNG</div>
    </div>
    <div style="clear: both;"></div>
</div>