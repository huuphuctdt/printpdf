<div class="pdf-body" style="font-size: 13px;">
    <div style="text-align: center; color: #475443; font-size: 25px; font-weight: bold; margin-top: 10px;">KẾT QUẢ XÉT NGHIỆM TẾ BÀO CTC</div>
    <div style="float: left; width: 70%">
        <div style="margin-left: 275px;">Ngày 30/05/2018</div>
    </div>
    <div style="float: left;">
        <div style="padding: 0 5px; border: 1px solid black; text-align: center; font-weight: bold;">Số làm pap's : 388/18FP</div>
    </div>
    <div style="clear: both;"></div>
    <div style="float: left; width: 50%;">
        <span style="font-weight: bold;">Họ tên: </span>
        <span style="font-weight: bold;">Nguyễn Văn A</span>
    </div>
    <div style="float: left; width: 20%">
        <span style="font-weight: bold;">Tuổi: 1976</span>
    </div>
    <div style="float: left;">
        <span style="font-weight: bold;">Số nhập viện: 18/014330</span>
    </div>
    <div style="clear: both;"></div>
    <div style="float: left;width: 70%;">
        <span style="font-weight: bold;">Địa chỉ: </span><span>123 Trần Hưng Đạo, Quận 5, TPHCM</span>
    </div>
    <div style="float: left;">
        <span style="font-weight: bold;">Số ĐT: </span><span>0123456789</span>
    </div>
    <div style="clear: both"></div>
    <div style="float: left; width: 70%">
        <span style="font-weight: bold;">Lý do thực hiện phết: </span>
    </div>
    <div style="float: left;">
        <span style="font-weight: bold;">Ngày lấy mẫu: </span><span>28/05/2018</span>
    </div>
    <div style="clear: both"></div>
    <div style="float: left; width: 70%">
        <span style="font-weight: bold;">BS chỉ định: </span>
        <span style="font-weight: bold;">TRAN THI KIM NGUYET</span>
    </div>
    <div style="float: left;">
        <span style="font-weight: bold;">Số Code: </span>
    </div>
    <div style="clear: both"></div>

    <div style="text-align: center; font-weight: bold; color: #0000ff; font-size: 15px;">KẾT QUẢ KHẢO SÁT TẾ BÀO HỌC CỔ TỬ CUNG</div>
    <div style="color: #0000ff; font-weight: bold;">Loại bệnh phẩm</div>
    <div style="float: left;width: 33%;">
        <input type="checkbox" name="" id=""> <span style="color: #556b2f; font-weight: bold;">ThinPrep</span>
    </div>
    <div style="float: left;width: 33%;">
        <input type="checkbox" name="" id="" checked="checked"> <span style="color: #556b2f; font-weight: bold;">LiquiPrep</span>
    </div>
    <div style="float: left">
        <input type="checkbox" name="" id=""> <span style="color: #556b2f; font-weight: bold;">Phết thường quy</span>
    </div>
    <div style="clear: both;"></div>
    <div style="float: left;width: 33%; margin-top: 10px;">
        <input type="checkbox" name="" id="" checked="checked"> <span>Đạt yêu cầu chẩn đoán</span>
    </div>
    <div style="float: left;">
        <input type="checkbox" name="" id=""> <span>Không đạt yêu cầu chẩn đoán vì</span>
    </div>
    <div style="clear: both;"></div>
    <div style="float: left;width: 33%; margin-top: 10px;">
        <input type="checkbox" name="" id="" checked="checked"> <span style="font-weight: bold;">KHÔNG TỔN THƯƠNG TRONG BIỂU MÔ HAY UNG THƯ</span>
    </div>
    <div style="float: left;width: 33%;">
        <input type="checkbox" name="" id="" > <span style="font-weight: bold;">BẤT THƯỜNG TB BIỂU MÔ</span>
    </div>
    <div style="float: left">
        <div><input type="checkbox" name="" id=""> <span style="font-weight: bold;">BẤT THƯỜNG KHÁC</span></div>
        <div><input type="checkbox" name="" id=""> <span>Tế bào nội mạc tử cung</span></div>
    </div>
    <div style="clear: both;"></div>
    <div style="float: left; width: 40%;">
        <div style="color: #0000ff;">BIẾN ĐỔI TẾ BÀO DO VI SINH</div>
    </div>
    <div style="float: left;">
        <div style="color: #0000ff; ">TẾ BÀO GAI</div>
    </div>
    <div style="clear: both;"></div>
    <div style="float: left; width: 40%;">
        <div><input type="checkbox" name="" id=""> <span>Trichomonas Vaginalis</span></div>
        <div><input type="checkbox" name="" id=""> <span>Nấm: candida sp</span></div>
        <div><input type="checkbox" name="" id=""> <span>Tạp trùng</span></div>
        <div><input type="checkbox" name="" id=""> <span>Actinomyces sp</span></div>
        <div><input type="checkbox" name="" id=""> <span>Herpes simplex virus</span></div>
    </div>
    <div style="float: left;">
        <div><input type="checkbox" name="" id=""> <span>Tế bào gai không điển hình ý nghĩa không xác định (ASC-US)</span></div>
        <div><input type="checkbox" name="" id=""> <span>Tế bào gai không điển hình, chưa loại trừ HSIL (ASC-H)</span></div>
        <div><input type="checkbox" name="" id=""> <span>Tổn thương trong thượng mô gai grade thấp(LSIL)</span></div>
        <div><input type="checkbox" name="" id=""> <span>Tổn thương trong thượng mô gai grade thấp(LSIL)+(HPV)</span></div>
        <div><input type="checkbox" name="" id=""> <span>Tổn thương trong biểu mô gai grade cao(HSIL)</span></div>
        <div><input type="checkbox" name="" id=""> <span>Carcinom tế bào gai (SCC)</span></div>
    </div>
    <div style="clear: both;"></div>
    <div style="float: left; width: 40%;">
        <div style="color: #0000ff;">BIẾN ĐỔI TẾ BÀO KHÁC</div>
    </div>
    <div style="float: left;">
        <div style="color: #0000ff;">TẾ BÀO TUYẾN</div>
    </div>
    <div style="clear: both;"></div>
    <div style="float: left; width: 40%;">
        <div><input type="checkbox" name="" id=""> <span>Tế bào biến đổi do viêm</span></div>
        <div><input type="checkbox" name="" id=""> <span>Tế bào biến đổi do xạ trị</span></div>
        <div><input type="checkbox" name="" id=""> <span>Tế bào biến đổi do vòng tránh thai</span></div>
        <div><input type="checkbox" name="" id=""> <span>Tế bào biến mô teo</span></div>
    </div>
    <div style="float: left;">
        <div><input type="checkbox" name="" id=""> <span>Tế bào tuyến không điển hình (AGC)</span></div>
        <div style="float: left; width: 33%; margin-left: 10px;">
            <div><input type="checkbox" name="" id=""> <span>cổ trong CTC</span></div>
        </div>
        <div style="float: left; width: 33%">
            <div><input type="checkbox" name="" id=""> <span>Nội mạc TC</span></div>
        </div>
        <div style="float: left">
            <div><input type="checkbox" name="" id=""> <span>Không xác định</span></div>
        </div>
        <div style="clear: both"></div>
        <div><input type="checkbox" name="" id=""> <span>Tế bào tuyến không điển hình nghĩ U</span></div>
        <div style="float: left; width: 33%; margin-left: 10px;">
            <div><input type="checkbox" name="" id=""> <span>cổ trong CTC</span></div>
        </div>
        <div style="float: left; width: 33%">
            <div><input type="checkbox" name="" id=""> <span>Nội mạc TC</span></div>
        </div>
        <div style="float: left">
            <div><input type="checkbox" name="" id=""> <span>Không xác định</span></div>
        </div>
        <div style="clear: both"></div>
        <div><input type="checkbox" name="" id=""> <span>Carcinom tuyến tại chổ (AIS)</span></div>
        <div><input type="checkbox" name="" id=""> <span>Carcinom tuyến xâm lấn</span></div>
    </div>
    <div style="clear: both;"></div>
    <div style="float: left; width: 50%;">
        <div><img height="180" width="270" class="" alt="" src="<?php echo base_url('assets/images/black.jpg'); ?>"></div>
    </div>
    <div style="float: left;">
        <div><img height="180" width="270" class="" alt="" src="<?php echo base_url('assets/images/black.jpg'); ?>"></div>
    </div>
    <div style="clear: both;"></div>
    <div><span style="font-weight: bold;">Kết luận: </span><span> Benign cellular change. (Tế bào biến đổi lành tính)</span></div>
    <div><span style="font-weight: bold;">Ghi chú</span></div>
    <div><span style="font-weight: bold;">Đề nghị</span></div>
    <div style="float: left;width: 40%;">&nbsp;</div>
    <div style="float: left">
        <div style="text-align: center; font-weight: bold;">Tp.HCM, Ngày 03 tháng 5 năm 2018</div>
        <div style="text-align: center; font-weight: bold;">BS GPB-TBH</div>
        <div style="text-align: center; font-weight: bold; margin-top: 80px;">DR, BUI NGOC DE</div>
    </div>
    <div style="clear: both;"></div>
</div>