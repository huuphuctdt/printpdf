<div class="pdf-body">
    <div style="float: left; width: 60%;">&nbsp;</div>
    <div style="float: left; text-align: center;">
        <div><span>Mã Số : </span><span>13/BV-01</span></div>
        <div><span>Số phiếu : </span><span>XQ-00388</span></div>
    </div>
    <div style="clear: both;"></div>
    <div style="float: left; width: 60%; padding-top: 20px;">
        <div style="font-weight: bold; font-size: 20px; text-align: center;">KẾT QUẢ X QUANG</div>
    </div>
    <div style="float: left; text-align: center;">
        <div style="margin-top: 15px;">
            <div><barcode code="<?=$barcode;?>" type="EAN128C" /></div>
            <?php if(isset($barcode) && $barcode != ""){ ?>
                <div><span style="font-weight: bold;">Mã số (Code): </span> <?=$barcode;?></div>
            <?php } ?>
        </div>
    </div>
    <div style="clear: both;"></div>
    <hr/>
    <div style="float: left; width: 70%;">
        <span>Họ và tên(Full name) : </span>
        <span>Nguyễn Văn A</span>
    </div>
    <div style="float: left;">
        <span>Tuổi(Age) : 1988</span>
    </div>
    <div style="clear: both;"></div>
    <div><span>Phòng XQuang (XRay No) : </span><span>Phòng XQUANG SỐ 12</span></div>
    <div><span>Địa chỉ (Address) : </span><span>123 Trần Hưng Đạo, Quận 5, TPHCM</span></div>
    <div><span>Chẩn đoán lâm sàng(Clinical Diagnosis) : </span><span></span></div>
    <div><span>BS Chỉ Định (Treatment Doctor) : </span><span>ĐINH THỊ HỒNG</span></div>
    <hr/>

    <div style="float: left; width: 30%">
        <div style="font-weight: bold;">
            Mô tả (Description) :
        </div>
    </div>
    <div style="float: left;">
        <div style="padding: 10px;">- Buồng tử cung hình tam giác, kích thước trung bình, bờ láng.</div>
        <div style="padding: 10px;">- Hai vòi trứng : </div>
        <div style="padding: 10px; margin-left: 15px;">* Phải: thuốc thoát, cotte (+).</div>
        <div style="padding: 10px; margin-left: 15px;">* Trái: thuốc thoát, cotte (+).</div>
    </div>
    <div style="clear: both;"></div>
    <div style="margin-top: 50px;">
        <span style="font-weight: bold;">
            Kết luận(Result):
        </span>
        <span>
            HAI VÒI TRỨNG THÔNG / BUỒNG TỬ CUNG BÌNH THƯỜNG.
        </span>
    </div>
    <div>
        <span style="font-weight: bold;">
            Đề ngị(Suggestion) :
        </span>
        <span></span>
    </div>
    <div style="float: left;width: 60%;">&nbsp;</div>
    <div style="float: left">
        <div style="text-align: center; font-weight: bold;">Tp.HCM, Ngày 03 tháng 5 năm 2018</div>
        <div style="text-align: center; font-weight: bold;">Bác sĩ X-Quang/XRay Dr</div>
        <div style="text-align: center; font-weight: bold; margin-top: 80px;">VÕ THỊ THU HẰNG</div>
    </div>
    <div style="clear: both;"></div>
</div>