<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<div class="pdf-header">
    <div>
        <div class="header-logo" style="float: left; width: 15%;margin-top-50px ;">
            <img class="svg-target" alt="alt-text" src="<?php echo base_url('assets/images/logo.png'); ?>">
        </div>
        <div class="header-title" style="margin-top-50px ; width: 75%;padding: 10px; text-align: center;">
            <div style="font-size: 20px; padding: 2px;font-weight: 900">BỆNH VIỆN PHỤ SẢN QUỐC TẾ SÀI GÒN</div>
            <div style="font-size: 15px; padding: 2px;color: #ff0000; font-style: italic;">SAIGON INTERNATIONAL OB-GYN. HOSPITAL</div>
            <div style="font-size: 15px; padding: 2px;">63 Bùi Thị Xuân, P.Phạm Ngũ Lão, Q.1, TP. Hồ Chí Minh</div>
            <div style="font-size: 15px; padding: 2px;">ĐT: 08 9990 9268 - 08 9990 9269 - 08 9990 9270 - 09 01770 009</div>
            <div style="font-size: 15px; padding: 2px;">Fax: (028) 3925 3184 - Email: info@sihospital.com.vn</div>
            <div style="font-size: 15px; padding: 2px;">Website: www.sihospital.com.vn</div>
        </div>
        <div style="clear: both;"></div>
    </div>
</div>