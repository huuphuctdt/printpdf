<div class="pdf-body" style="width: 100%;">
    <div style="width: 70%; float: left; text-align: center;">
        <h2 style="margin-top: 25px;">PHIẾU KHÁM KHH GIA ĐÌNH</h2>
    </div>
    <div style="float: left; text-align: center;">
        <?php if(isset($barcode) && $barcode != ""){ ?>
            <div><span style="font-weight: bold;">Mã số: </span> <?=$barcode;?></div>
        <?php } ?>
        <div><barcode code="<?=$barcode;?>" type="EAN128C" /></div>
    </div>
    <div style="clear: both;"></div>

    <div style="float: left; width: 40%; text-align: left;">
        <div style="font-weight: bold; font-size: 15px; padding-top: 5px;">I. HÀNH CHÍNH</div>
    </div>
    <div style="float: left; text-align: right;">
        <div style="font-style: italic; font-size: 15px; padding-top: 5px;">Thời gian khám lâm sàng cam kết trong khoảng 15 phút.</div>
    </div>
    <div style="clear: both;"></div>

    <div style="width: 40%; float: left; padding-top: 5px;">
        1. Họ và tên: Nguyễn Thị A
    </div>
    <div style="float: left;  padding-top: 5px;">
        <span>2. Sinh ngày: 07/12/1993</span>
        <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tuổi: 25</span>
    </div>
    <div style="clear: both;"></div>
    <div style="width: 40%; float: left; padding-top: 5px;">
        3. Giới tính: Nữ
    </div>
    <div style="float: left; padding-top: 5px;">
        4. Nghề nghiệp: Nhân viên
    </div>
    <div style="clear: both;"></div>
    <div style="width: 40%; float: left; padding-top: 5px;">
        5. Dân tộc: Kinh
    </div>
    <div style="float: left; padding-top: 5px;">
        6. Quốc tịch: Việt Nam
    </div>
    <div style="clear: both;"></div>
    <div style="padding-top: 5px;">
        <div style="display: inline-block;">
            7. Địa chỉ: 123 Trần Hưng Đạo, phường 13, Quận 1, TP.HCM
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            Điện thoại: 0352407555
        </div>
    </div>
    <div style="clear: both;"></div>
    <div style="padding-top: 5px;">
        8. Đến khám bệnh lúc: 09 giờ 25 phút, Ngày 23 tháng 11 năm 2017
    </div>
    <div style="font-weight: bold; font-size: 15px; padding-top: 5px;">II. HỎI BỆNH - THĂM KHÁM - ĐIỀU TRỊ</div>

    <div  style="margin-top: 10px;"><span>- Lý do vào viện : </span></div>
    <div style="float: left; width: 30%; margin-top: 10px;">
        <span>1. Khám lâm sàng:</span>
    </div>
    <div style="float: left;">
        <div>PARA 1021</div>
        <div>KC 18/9/2017</div>
        <div>ÂM ĐẠO: ÍT KHÍ HƯ</div>
        <div>CTC: ĐÓNG TCC > BT</div>
        <div>2PP: KO CHẠM</div>
        <div>XIN BỎ THAI</div>
        <div>A Đ ÍT KHÍ HƯ</div>
        <div>CTC ĐÓNG</div>
        <div>TTC > BÌNH THƯỜNG</div>
        <div>2 P KHÔNG CHẠM</div>
    </div>
    <div style="clear: both;"></div>
    <div style="float: left; width: 30%; margin-top: 10px;">
        <span>2. Cận lâm sàng:</span>
    </div>
    <div style="float: left;">
        <div>SA: 1 THAI + TC 8 TUẦN XNM: NHÓM MÁU :O RH+BETA</div>
        <div>HCG: 158265CTM : MCH GIẢM MCH : GIẢM</div>
    </div>
    <div style="clear: both;"></div>
    <div style="float: left; width: 30%; margin-top: 10px;">
        <span>3. Chẩn đoán, chỉ định:</span>
    </div>
    <div style="float: left;">
        <div>THAI 8 TUẦN / XIN KH / HÚT THAI + VIÊM ÂM ĐẠO TẠP TRÙNG (O04)</div>
    </div>
    <div style="clear: both;"></div>
    <div style="margin-top: 10px;"><span>4. Lời dặn: </span></div>
    <div style="margin-top: 10px;"><span>5. Tái khám: </span></div>

    <div style="float: left;width: 60%;">&nbsp;</div>
    <div style="float: left">
        <div style="text-align: center; font-weight: bold;">Ngày 23 tháng 11 năm 2017</div>
        <div style="text-align: center; font-weight: bold;">BÁC SĨ KHÁM BỆNH</div>
        <div style="text-align: center; font-weight: bold; margin-top: 80px;">ĐẶNG THỊ TUYẾT NGA</div>
    </div>
    <div style="clear: both;"></div>
</div>