<div class="pdf-body" style="font-size: 13px;">
    <div style="text-align: center; color: #475443; font-size: 25px; font-weight: bold; margin-top: 10px;">PHIẾU THANH TOÁN RA VIỆN</div>
    <div>
        <div style="text-align: right; font-weight: bold;">Ngày In: 25/05/2018</div>
    </div>
    <div style="clear: both;"></div>
    <div style="float: left; width: 50%;">
        <span>Mã Bệnh Án: </span>
        <span style="font-weight: bold;">BASK-039746</span>
    </div>
    <div style="float: left;">
        <span>Số nhập viện: </span><span style="font-weight: bold;">18-1157</span>
    </div>
    <div style="clear: both;"></div>
    <div style="float: left; width: 50%;">
        <span>Họ tên: </span>
        <span style="font-weight: bold;">Nguyễn Văn A</span>
    </div>
    <div style="float: left; width: 20%">
        <span>Năm Sinh: </span><span style="font-weight: bold;">1956</span>
    </div>
    <div style="clear: both;"></div>
    <div>
        <span>Địa chỉ: </span><span style="font-weight: bold;">123 Trần Hưng Đạo, Quận 5, TPHCM</span>
    </div>
    <div>
        <span>Chẩn đoán nhập viện: </span>
        <span style="font-weight: bold;">thai 38 tuần 1 ngày / VMC</span>
    </div>

    <table style="width: 100%;" border="1">
        <thead>
            <tr>
                <th>STT</th>
                <th>Loại Dịch Vụ</th>
                <th>Số Lượng</th>
                <th>Thành Tiền</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td style="text-align: center;">1</td>
                <td>Thuốc - Y dụng cụ</td>
                <td style="text-align: right;">318</td>
                <td style="text-align: right;">6,428,000</td>
            </tr>
            <tr>
                <td style="text-align: center;">2</td>
                <td>BCG (Lao)</td>
                <td style="text-align: right;">1</td>
                <td style="text-align: right;">70,000</td>
            </tr>
            <tr>
                <td style="text-align: center;">3</td>
                <td>Định danh nhóm máu AB0h-Rh(Gelcard)</td>
                <td style="text-align: right;">1</td>
                <td style="text-align: right;">150,000</td>
            </tr>
            <tr>
                <td style="text-align: center;">4</td>
                <td>Siêu âm bụng</td>
                <td style="text-align: right;">1</td>
                <td style="text-align: right;">200,000</td>
            </tr>
            <tr>
                <td style="text-align: center;">5</td>
                <td>Tầm soát khiếm thính</td>
                <td style="text-align: right;">1</td>
                <td style="text-align: right;">200,000</td>
            </tr>
            <tr>
                <td style="text-align: center;">6</td>
                <td>XN Sàng lọc sau sinh</td>
                <td style="text-align: right;">1</td>
                <td style="text-align: right;">600,000</td>
            </tr>
            <tr>
                <td style="text-align: center;">7</td>
                <td>Khám tiền mê</td>
                <td style="text-align: right;">1</td>
                <td style="text-align: right;">500,000</td>
            </tr>
            <tr>
                <td style="text-align: center;">8</td>
                <td>Thăm khám bé tại phòng (sanh mổ)</td>
                <td style="text-align: right;">1</td>
                <td style="text-align: right;">500,000</td>
            </tr>
            <tr>
                <td style="text-align: center;">9</td>
                <td>Hồi sức 1/2 Ngày</td>
                <td style="text-align: right;">1</td>
                <td style="text-align: right;">800,000</td>
            </tr>
            <tr>
                <td style="text-align: center;">10</td>
                <td>Yêu cầu bác sĩ (sanh mổ)</td>
                <td style="text-align: right;">1</td>
                <td style="text-align: right;">1,500,000</td>
            </tr>
            <tr>
                <td style="text-align: center;">11</td>
                <td>Mổ lấy thai, vết mổ cũ</td>
                <td style="text-align: right;">1</td>
                <td style="text-align: right;">7,500,000</td>
            </tr>
            <tr>
                <td style="text-align: center;">12</td>
                <td>Điều trị nội trú</td>
                <td style="text-align: right;">5</td>
                <td style="text-align: right;">10,000,000</td>
            </tr>
            <tr>
                <td style="text-align: center;">13</td>
                <td>Thuốc - Y dụng cụ bé</td>
                <td style="text-align: right;">15</td>
                <td style="text-align: right;">444,000</td>
            </tr>
            <tr>
                <td style="text-align: center;">14</td>
                <td>Hepavex Gene 10mcg</td>
                <td style="text-align: right;">1</td>
                <td style="text-align: right;">75,000</td>
            </tr>
            <tr>
                <td style="text-align: center;">15</td>
                <td>Glycemie (Glucose)</td>
                <td style="text-align: right;">1</td>
                <td style="text-align: right;">100,000</td>
            </tr>
            <tr>
                <td style="text-align: center;">16</td>
                <td>Dưỡng Nhi</td>
                <td style="text-align: right;">1</td>
                <td style="text-align: right;">500,000</td>
            </tr>
            <tr>
                <td colspan="3" style="font-weight: bold; text-align: center;">Tổng cộng dịch vụ</td>
                <td style="text-align: right;font-weight: bold;">29,567,000</td>
            </tr>
            <tr>
                <td colspan="3" style="font-weight: bold; text-align: center;">Tạm ứng</td>
                <td style="text-align: right;font-weight: bold;">- 22,000,000</td>
            </tr>
            <tr>
                <td colspan="3" style="font-weight: bold; text-align: center;">Thanh Toán</td>
                <td style="text-align: right;font-weight: bold;">7,567,000</td>
            </tr>
        </tbody>
    </table>



    <div style="float: left; width: 30%; margin-top: 15px;">
        <div style="text-align: center; font-weight: bold;">Khách hàng</div>
        <div style="text-align: center;">Xác nhận</div>
    </div>
    <div style="float: left; width: 30%">
        <div style="text-align: center; font-weight: bold;">Người thu tiền</div>
        <div style="text-align: center;">Xác nhận</div>
    </div>
    <div style="float: left;">
        <div style="text-align: center;">TPHCM, Ngày 25 tháng 5 năm 2018</div>
        <div style="text-align: center; font-weight: bold;">Người lập phiếu</div>
    </div>
    <div style="clear: both;"></div>
</div>