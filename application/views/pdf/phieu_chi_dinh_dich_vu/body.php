<div class="pdf-body">
    <div style="float: left;width: 50%; ">
        <div style="font-weight: bold; font-size: 24px;text-align: center;">PHIẾU CHỈ ĐỊNH DỊCH VỤ</div>
        <div style="font-weight: bold; text-align: center; margin-top: 10px;">Ngày: 25/05/2018</div>
        <div style="margin-top: 10px;">
            <span style="font-weight: bold;">Mã KH: 14/048129</span>
            <span style="font-weight: bold;">Mã Phụ: 30903/14</span>
        </div>
        <div style="font-weight: bold;margin-top: 10px;">Họ và tên: CHU THỊ THỦY NGỌC</div>
    </div>
    <div style="float: left;width: 50%; text-align: center;">
        <div><barcode code="<?=$barcode;?>" type="EAN128C" /></div>
        <div><span style="font-weight: bold;">Mã số: </span> <?=$barcode;?></div>
        <div style="margin-top: 12px;"><span style="font-weight: bold;">Năm sinh:</span> 1989</div>
        <div style="margin-top: 12px;"><span style="font-weight: bold;">Giới tính:</span> Nữ</div>
    </div>
    <div style="clear: both;"></div>
    <div><span style="font-weight: bold;margin-top: 10px;">Địa chỉ:</span> 13 LÔ C, CƯ XÁ LẠC LONG QUÂN, ĐƯỜNG NGUYÊN VĂN PHÚ, PHƯỜNG 5, QUẬN 11, TPHCM</div>
    <div><span style="font-weight: bold;margin-top: 10px;">Chẩn đoán:</span> rong kinh???</div>
    <table style="width: 100%; text-align: center;margin-top: 10px;">
        <tr>
            <th>STT</th>
            <th>Tên chỉ định</th>
            <th>Nơi thực hiện</th>
            <th>Số lượng</th>
        </tr>
        <tr>
            <td>1</td>
            <td>Pap Thin-Prep</td>
            <td></td>
            <td>1</td>
        </tr>
        <tr>
            <td>2</td>
            <td>Soi & nhuộm dịch âm đạo</td>
            <td>Phòng xét nghiệm số 13</td>
            <td>1</td>
        </tr>
    </table>

    <div style="float: left;width: 60%;">&nbsp;</div>
    <div style="float: left">
        <div style="text-align: center; font-weight: bold;">Ngày:25 tháng 05 năm 2018</div>
        <div style="text-align: center; font-weight: bold;">Bác sĩ khám bệnh</div>
        <div style="text-align: center; font-weight: bold; margin-top: 80px;">TRẦN ĐOÀN PHƯƠNG TRÂN</div>
    </div>
    <div style="clear: both;"></div>
</div>