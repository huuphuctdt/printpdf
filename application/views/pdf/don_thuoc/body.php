<div class="pdf-body">
    <div style="float: left;width: 50%; ">
        <div style="font-weight: bold; font-size: 30px;text-align: center;">ĐƠN THUỐC</div>
        <div style="font-weight: bold; text-align: center; margin-top: -3px;">Ngày: 04/05/2018</div>
        <div style="margin-top: 15px;">
            <span style="font-weight: bold;">Mã BN: 13/054482</span>
        </div>
        <div style="font-weight: bold;margin-top: 10px;">Họ và tên: CHU THỊ THỦY NGỌC</div>
    </div>
    <div style="float: left;width: 50%; text-align: center;">
        <div><barcode code="<?=$barcode;?>" type="EAN128C" /></div>
        <div><span style="font-weight: bold;">Mã số (Code): </span> <?=$barcode;?></div>
        <div style="margin-top: 13px;"><span style="font-weight: bold;">Năm sinh:</span> 1983</div>
        <div style="margin-top: 12px;"><span style="font-weight: bold;">Giới tính:</span> Nữ</div>
    </div>
    <div style="clear: both;"></div>
    <div style="margin-top: 10px;"><span style="font-weight: bold;">Địa chỉ:</span> 13 K1 KHU DÂN CƯ TÂN QUY ĐÔNG, PHƯỜNG TÂN PHONG, QUẬN 7, TPHCM</div>
    <div style="margin-top: 10px;"><span style="font-weight: bold;margin-top: 10px;">Chẩn đoán:</span> THAI 32 TUẦN 1 NGÀY </div>
    <table style="width: 100%; text-align: center;margin-top: 10px;">
        <tr>
            <td style="width: 5%;">1</td>
            <td style="width: 20%; text-align: left;">Hemopoly 5ml</td>
            <td style="width: 55%; text-align: left;"><br><br><br>Uống ngày 1 ống</td>
            <td style="width: 20%;">40 Ống</td>
        </tr>
        <tr>
            <td style="width: 5%;">2</td>
            <td style="width: 20%; text-align: left;">Kiddiecal</td>
            <td style="width: 55%; text-align: left;"><br><br><br>Uống ngày 2 lần mỗi lần 1 viên</td>
            <td style="width: 20%;">60 Viên</td>
        </tr>
    </table>

    <div style="float: left;width: 60%;">&nbsp;</div>
    <div style="float: left">
        <div style="text-align: center; font-weight: bold;">Tp.HCM, Ngày 25 tháng 5 năm 2018</div>
        <div style="text-align: center; font-weight: bold;">Bác sĩ khám bệnh</div>
        <div style="text-align: center; font-weight: bold; margin-top: 80px;">NGUYỄN THỊ THU HỒNG</div>
    </div>
    <div style="clear: both;"></div>
</div>