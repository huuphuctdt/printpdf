<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<div class="pdf-header">
    <div>
        <div class="header-logo" style="float: left; width: 15%;">
            <img class="svg-target" alt="alt-text" src="<?php echo base_url('assets/images/logo.png'); ?>">
        </div>
        <div class="header-title" style="margin: auto;width: 75%;padding: 10px; text-align: center;">
            <div style="font-size: 20px; padding: 2px;font-weight: 900">BỆNH VIỆN PHỤ SẢN QUỐC TẾ SÀI GÒN</div>
            <div style="font-size: 15px; padding: 2px;color: #ff0000; font-style: italic;">SAIGON INTERNATIONAL OB-GYN. HOSPITAL</div>
            <div style="font-size: 15px; padding: 2px;">63 Bùi Thị Xuân, P.Phạm Ngũ Lão, Q.1, TP. Hồ Chí Minh</div>
            <div style="font-size: 15px; padding: 2px;">ĐT: 08 9990 9268 - 08 9990 9269 - 08 9990 9270 - 09 01770 009</div>
            <div style="font-size: 15px; padding: 2px;">Fax: (028) 3925 3184 - Email: info@sihospital.com.vn</div>
            <div style="font-size: 15px; padding: 2px;">Website: www.sihospital.com.vn</div>
        </div>
        <div style="clear: both;"></div>
    </div>
    <div style="margin: auto;width: 75%;text-align: center;">
        <hr/>
    </div>
</div>
<div class="pdf-body">
    <div style="float: left;width: 50%; ">
        <div style="font-weight: bold; font-size: 24px;text-align: center;">PHIẾU CHỈ ĐỊNH DỊCH VỤ</div>
        <div style="font-weight: bold; text-align: center; margin-top: 10px;">Ngày: 25/05/2018</div>
        <div style="margin-top: 10px;">
            <span style="font-weight: bold;">Mã KH: 14/048129</span>
            <span style="font-weight: bold;">Mã Phụ: 30903/14</span>
        </div>
        <div style="font-weight: bold;margin-top: 10px;">Họ và tên: CHU THỊ THỦY NGỌC</div>
    </div>
    <div style="float: left;width: 50%; text-align: center;">
        <div><barcode code="<?=$barcode;?>" type="EAN128C" /></div>
        <div><span style="font-weight: bold;">Mã số: </span> <?=$barcode;?></div>
        <div style="margin-top: 12px;"><span style="font-weight: bold;">Năm sinh:</span> 1989</div>
        <div style="margin-top: 12px;"><span style="font-weight: bold;">Giới tính:</span> Nữ</div>
    </div>
    <div style="clear: both;"></div>
    <div><span style="font-weight: bold;margin-top: 10px;">Địa chỉ:</span> 13 LÔ C, CƯ XÁ LẠC LONG QUÂN, ĐƯỜNG NGUYÊN VĂN PHÚ, PHƯỜNG 5, QUẬN 11, TPHCM</div>
    <div><span style="font-weight: bold;margin-top: 10px;">Chẩn đoán:</span> rong kinh???</div>
    <table style="width: 100%; text-align: center;margin-top: 10px;">
        <tr>
            <th>STT</th>
            <th>Tên chỉ định</th>
            <th>Nơi thực hiện</th>
            <th>Số lượng</th>
        </tr>
        <tr>
            <td>1</td>
            <td>Pap Thin-Prep</td>
            <td></td>
            <td>1</td>
        </tr>
        <tr>
            <td>2</td>
            <td>Soi & nhuộm dịch âm đạo</td>
            <td>Phòng xét nghiệm số 13</td>
            <td>1</td>
        </tr>
    </table>
    <div style="text-align: right;margin-top: 10px;">Tp.HCM, Ngày 25 tháng 5 năm 2018</div>
    <div style="font-weight: bold;text-align: right;">Bác sĩ khám bệnh</div>
    <div style="font-weight: bold; font-style: italic;text-align: right;margin-top: 150px;">TRẦN ĐOÀN PHƯƠNG TRÂN</div>
</div>
<div class="pdf-footer">
    <div style="position: fixed;bottom: 0;width: 100%;">Lưu ý: Quý khách vui lòng mang theo phiếu này khi đến khám.</div>
</div>