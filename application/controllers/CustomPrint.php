<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(FCPATH . '/vendor/autoload.php');

use \Picqer\Barcode\BarcodeGeneratorHTML;
use \Mpdf\Mpdf;

class CustomPrint extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library(['printpdf/PrintPDF']);
    }

    public function index(){

//        $html = $this->getTemplatePhieuKhamPhuKhoa(['barcode' => '1001-0022662']);
//        $pdf = PrintPDF::printPDF($html,'phieu_kham_phu_khoa.pdf');
//        $html = $this->getTemplateKetQuaSieuAm(['barcode' => '1001-0022662']);
//        $pdf = PrintPDF::printPDF($html,'ket_qua_sieu_am.pdf');
//        $html = $this->getTemplatePhieuSoiCoTuCung(['barcode' => '1001-0022662']);
//        $pdf = PrintPDF::printPDF($html,'phieu_soi_co_tu_cung.pdf');
//        $html = $this->getTemplateKetQuaSieuAm4D(['barcode' => '1001-0022662']);
//        $pdf = PrintPDF::printPDF($html,'ket_qua_sieu_am_4d.pdf');
//        $html = $this->getTemplateXetNghiemTeBaoCTC(['barcode' => '1001-0022662']);
//        $pdf = PrintPDF::printPDF($html,'xet_nghiem_te_bao_ctc.pdf');
//        $html = $this->getTemplateKetQuaXQuang(['barcode' => '1001-0022662']);
//        $pdf = PrintPDF::printPDF($html,'ket_qua_x_quang.pdf');
//        $html = $this->getTemplateKetQuaNhuAnh(['barcode' => '1001-0022662']);
//        $pdf = PrintPDF::printPDF($html,'ket_qua_nhu_anh.pdf');
//        $html = $this->getTemplatePhieuChiDinhDichVu(['barcode' => '1002-0070330']);
//        $pdf = PrintPDF::printPDF($html,'phieu_chi_dinh_dich_vu.pdf');
//        $html = $this->getTemplateDonThuoc(['barcode' => '1001-0022662', 'ngay_tai_kham' => '26/05/2018']);
//        $pdf = PrintPDF::printPDF($html,'don_thuoc.pdf');
//        $html = $this->getTemplateTPCN(['barcode' => '1001-0081558']);
//        $pdf = PrintPDF::printPDF($html,'don_thuoc_tpcn.pdf');
//        $html = $this->getTemplatePhieuKhamThai(['barcode' => '1001-0081558']);
//        $pdf = PrintPDF::printPDF($html,'phieu_kham_thai.pdf');
//        $html = $this->getTemplatePhieuKhamBenhManKinh(['barcode' => '1001-0081558']);
//        $pdf = PrintPDF::printPDF($html,'phieu_kham_benh_man_kinh.pdf');
//        $html = $this->getTemplateKetQuaChocHutTeBaoBangKimNho(['barcode' => '1001-0081558']);
//        $pdf = PrintPDF::printPDF($html,'ket_qua_choc_hut_te_bao_bang_kim_nho.pdf');
//        $html = $this->getTemplateKetQuaChanDoanGiaiPhauBenh(['barcode' => '1001-0081558']);
//        $pdf = PrintPDF::printPDF($html,'ket_qua_chan_doan_giai_phai_benh.pdf');
//        $html = $this->getTemplatePhieuKhamKHHGD(['barcode' => '1001-0081558']);
//        $pdf = PrintPDF::printPDF($html,'phieu_kham_khhgd.pdf');
//        $html = $this->getTemplatePhieuKhamHiemMuon(['barcode' => '1001-0081558']);
//        $pdf = PrintPDF::printPDF($html,'phieu_kham_hiem_muon.pdf');
//        $html = $this->getTemplatePhieuKhamNhu(['barcode' => '1001-0081558']);
//        $pdf = PrintPDF::printPDF($html,'phieu_kham_nhu.pdf');
        $html = $this->getTemplatePhieuThanhToanRaVien(['barcode' => '1001-0081558']);
        $pdf = PrintPDF::printPDF($html,'phieu_thanh_toan_ra_vien.pdf');
        return $pdf;
    }

    public function getTemplatePhieuChiDinhDichVu($dataInput = array()){
        $html = '';
        $html .= $this->load->view('pdf/header','',true);
        $html .= $this->load->view('pdf/phieu_chi_dinh_dich_vu/body',['barcode' => isset($dataInput['barcode']) ? $dataInput['barcode'] : '' ],true);
        $html .= $this->load->view('pdf/footer','',true);
        return $html;
    }

    public function getTemplateTPCN($dataInput = array()){
        $html = '';
        $html .= $this->load->view('pdf/header','',true);
        $html .= $this->load->view('pdf/tpcn/body',['barcode' => isset($dataInput['barcode']) ? $dataInput['barcode'] : ''],true);
        $html .= $this->load->view('pdf/footer','',true);
        return $html;
    }

    public function getTemplateDonThuoc($dataInput = array()){
        $html = '';
        $html .= $this->load->view('pdf/header','',true);
        $html .= $this->load->view('pdf/don_thuoc/body',['barcode' => isset($dataInput['barcode']) ? $dataInput['barcode'] : '' ],true);
        $html .= $this->load->view('pdf/footer',['ngay_tai_kham' => isset($dataInput['ngay_tai_kham']) ? $dataInput['ngay_tai_kham'] : ''],true);
        return $html;
    }

    public function getTemplatePhieuSoiCoTuCung($dataInput = array()){
        $html = '';
        $html .= $this->load->view('pdf/header','',true);
        $html .= $this->load->view('pdf/phieu_soi_co_tu_cung/body',['barcode' => isset($dataInput['barcode']) ? $dataInput['barcode'] : ''],true);
        return $html;
    }

    public function getTemplateKetQuaSieuAm($dataInput = array()){
        $html = '';
        $html .= $this->load->view('pdf/ket_qua_sieu_am/body',['barcode' => isset($dataInput['barcode']) ? $dataInput['barcode'] : ''],true);
        return $html;
    }

    public function getTemplatePhieuKhamPhuKhoa($dataInput = array()){
        $html = '';
        $html .= $this->load->view('pdf/header_new','',true);
        $html .= $this->load->view('pdf/phieu_kham_phu_khoa/body',['barcode' => isset($dataInput['barcode']) ? $dataInput['barcode'] : ''],true);
        return $html;
    }

    public function getTemplateKetQuaSieuAm4D($dataInput = array()){
        $html = '';
        $html .= $this->load->view('pdf/ket_qua_sieu_am_4d/body',['barcode' => isset($dataInput['barcode']) ? $dataInput['barcode'] : ''],true);
        return $html;
    }

    public function getTemplateKetQuaXQuang($dataInput = array()){
        $html = '';
        $html .= $this->load->view('pdf/ket_qua_x_quang/body',['barcode' => isset($dataInput['barcode']) ? $dataInput['barcode'] : ''],true);
        return $html;
    }

    public function getTemplateXetNghiemTeBaoCTC($dataInput = array()){
        $html = '';
        $html .= $this->load->view('pdf/header_new','',true);
        $html .= $this->load->view('pdf/xet_nghiem_te_bao_ctc/body',['barcode' => isset($dataInput['barcode']) ? $dataInput['barcode'] : ''],true);
        return $html;
    }

    public function getTemplateKetQuaNhuAnh($dataInput = array()){
        $html = '';
        $html .= $this->load->view('pdf/ket_qua_nhu_anh/body',['barcode' => isset($dataInput['barcode']) ? $dataInput['barcode'] : ''],true);
        return $html;
    }

    public function getTemplatePhieuKhamThai($dataInput = array()){
        $html = '';
        $html .= $this->load->view('pdf/header_new','',true);
        $html .= $this->load->view('pdf/phieu_kham_thai/body',['barcode' => isset($dataInput['barcode']) ? $dataInput['barcode'] : ''],true);
        return $html;
    }

    public function getTemplatePhieuKhamBenhManKinh($dataInput = array()){
        $html = '';
        $html .= $this->load->view('pdf/header_new','',true);
        $html .= $this->load->view('pdf/phieu_kham_benh_man_kinh/body',['barcode' => isset($dataInput['barcode']) ? $dataInput['barcode'] : ''],true);
        return $html;
    }

    public function getTemplateKetQuaChocHutTeBaoBangKimNho($dataInput = array()){
        $html = '';
        $html .= $this->load->view('pdf/header_new','',true);
        $html .= $this->load->view('pdf/ket_qua_choc_hut_te_bao_bang_kim_nho/body',['barcode' => isset($dataInput['barcode']) ? $dataInput['barcode'] : ''],true);
        return $html;
    }

    public function getTemplateKetQuaChanDoanGiaiPhauBenh($dataInput = array()){
        $html = '';
        $html .= $this->load->view('pdf/header_new','',true);
        $html .= $this->load->view('pdf/ket_qua_chan_doan_giai_phau_benh/body',['barcode' => isset($dataInput['barcode']) ? $dataInput['barcode'] : ''],true);
        return $html;
    }

    public function getTemplatePhieuKhamKHHGD($dataInput = array()){
        $html = '';
        $html .= $this->load->view('pdf/header_new','',true);
        $html .= $this->load->view('pdf/phieu_kham_khhgd/body',['barcode' => isset($dataInput['barcode']) ? $dataInput['barcode'] : ''],true);
        return $html;
    }

    public function getTemplatePhieuKhamHiemMuon($dataInput = array()){
        $html = '';
        $html .= $this->load->view('pdf/header_new','',true);
        $html .= $this->load->view('pdf/phieu_kham_hiem_muon/body',['barcode' => isset($dataInput['barcode']) ? $dataInput['barcode'] : ''],true);
        return $html;
    }

    public function getTemplatePhieuKhamNhu($dataInput = array()){
        $html = '';
        $html .= $this->load->view('pdf/header_new','',true);
        $html .= $this->load->view('pdf/phieu_kham_nhu/body',['barcode' => isset($dataInput['barcode']) ? $dataInput['barcode'] : ''],true);
        return $html;
    }

    public function getTemplatePhieuThanhToanRaVien($dataInput = array()){
        $html = '';
        $html .= $this->load->view('pdf/header_new','',true);
        $html .= $this->load->view('pdf/phieu_thanh_toan_ra_vien/body',['barcode' => isset($dataInput['barcode']) ? $dataInput['barcode'] : ''],true);
        return $html;
    }
}